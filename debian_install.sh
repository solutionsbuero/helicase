# drop privileges back to non-root user if we got here with sudo
depriv() {
    if [ -z "$SUDO_USER" ]; then
        "$@"
    else
        sudo -u "$SUDO_USER" -- "$@"
    fi
}


if [ $(id -u) != 0 ]; then
    echo "You need to run this script as root."
    exit
fi

echo "\nGUI OR SERVER?"
echo "This setup can create a launcher for the webview-based GUI of Helicase on graphical systems or install a systemd service to launch Helicase as a deamon on servers (but not both!)."

read -p "Create Helicase launcher (y/n)?" choice
case "$choice" in
    y|Y|yes|Yes|YES ) CREATE_LAUNCHER=true;;
    * ) CREATE_LAUNCHER=false;;
esac

if [ "$CREATE_LAUNCHER" = false ]; then

    read -p "Create systemd service (y/n)?" choice
    case "$choice" in
        y|Y|yes|Yes|YES ) CREATE_DAEMON=true;;
        * ) CREATE_DAEMON=false;;
    esac

    read -p "Create dedicated helicase user (called 'helicase', RECOMMENDED!) (y/n)?" choice
    case "$choice" in
        y|Y|yes|Yes|YES ) CREATE_USER=true;;
        * ) CREATE_USER=false;;
    esac

fi

echo "\nINSTALLING DEPENDENCIES..."

apt-get install -y \
    mariadb-client \
    rsync \
    git \
    python3.7-dev \
    python3.7-venv

echo "\nCLONING REPO..."

git clone https://gitlab.com/solutionsbuero/helicase.git /opt/helicase

echo "\nCREATING VIRTUAL ENVIRONMENT..."

python3.7 -m venv /opt/helicase/venv

echo "\nINSTALLING APPLICATION AND REQUIREMENTS..."

/opt/helicase/venv/bin/pip3 install --upgrade setuptools
/opt/helicase/venv/bin/pip3 install --upgrade pip

if [ "$CREATE_LAUNCHER" = true ]; then

    echo "\nCREATING LAUNCHER..."
    cp /opt/helicase/helicase.desktop /usr/share/applications/
    /opt/helicase/venv/bin/pip3 install -r /opt/helicase/requirements_gui.txt /opt/helicase

fi

if [ "$CREATE_USER" = true ]; then
    adduser helicase --disabled-password
    sudo -u helicase mkdir /home/helicase/.config
fi

if [ "$CREATE_DAEMON" = true ]; then

    echo "\nCREATING DAEMON..."
    cp /opt/helicase/helicase.service /etc/systemd/system/
    systemctl enable helicase
    /opt/helicase/venv/bin/pip3 install -r /opt/helicase/requirements_server.txt /opt/helicase

fi

echo "\n...ALL DONE!\n"
