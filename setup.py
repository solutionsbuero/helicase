from distutils.core import setup
from setuptools import find_packages

setup(
	name='Helicase',
	version='0.2.3',
	description='Helicase backup system',
	author='Genossenschaft Solutionsbüro',
	author_email='info@buero.io',
	url='https://buero.io',
	packages=find_packages(),
	include_package_data=True,
)
