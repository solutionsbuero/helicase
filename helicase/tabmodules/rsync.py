import json
import subprocess
from peewee import fn
from typing import List, Optional
from playhouse.shortcuts import model_to_dict
from helicase.database.models import RSync, RunProtocol


class RSyncRunner:

	USUAL_PATTERNS = [
		{"human": "Usual SSH setup", "cmd": "--delete-after -rltDe ssh"},
		{"human": "local copy", "cmd": "-a --delete-after"},
		{"human": "Home dir over SSH", "cmd": "--delete-after --exclude=.gvfs --exclude=.cache --exclude=.dbus -rltDe ssh"}
	]

	@staticmethod
	def list_rsync() -> List[RSync]:
		return RSync.select().order_by(fn.Lower(RSync.human).asc())

	@staticmethod
	def get_rsync(rsyncid: int) -> RSync:
		return RSync.select().where(RSync.id == rsyncid).get()

	@staticmethod
	def get_rsync_json(rsyncid: int) -> str:
		query = RSync.select().where(RSync.id == rsyncid).get()
		return json.dumps(model_to_dict(query))

	@staticmethod
	def edit_rsync(formdata: dict, rsyncid: Optional[int] = None):
		if rsyncid is None:
			RSync.create(
				human=formdata["human"],
				flags=formdata["flags"],
				source=formdata["source"],
				destination=formdata["destination"]
			)
		else:
			rsync = RSync.get(RSync.id == rsyncid)
			rsync.human = formdata["human"]
			rsync.flags = formdata["flags"]
			rsync.source = formdata["source"]
			rsync.destination = formdata["destination"]
			rsync.save()

	@staticmethod
	def rm_rsync(rsyncid: int) -> Optional[list]:
		rsync = RSync.get(RSync.id == rsyncid)
		in_jobs = [j.job_id.human for j in rsync.jobsassoc]
		if len(in_jobs) > 0:
			return in_jobs
		protocol_entries = RunProtocol.select().where(RunProtocol.rsync_id == rsyncid)
		for j in protocol_entries:
			j.delete_instance()
		rsync.delete_instance(recursive=True)

	@staticmethod
	def assemble_cmd(rsync_entry: RSync) -> str:
		cmd = "rsync {} {} {}".format(
			rsync_entry.flags,
			rsync_entry.source,
			rsync_entry.destination
		)
		return cmd

	@staticmethod
	def run_rsync_sync(rsyncid: int) -> (int, str, str):
		rsyncdata = RSync.select().where(RSync.id == rsyncid).get()
		cmd = RSyncRunner.assemble_cmd(rsyncdata)
		sync_process = subprocess.Popen(cmd.split(), stderr=subprocess.PIPE, stdout=subprocess.PIPE)
		stdout, stderr = sync_process.communicate()
		rc = sync_process.returncode
		return rc, stdout, stderr.decode()


if __name__ == "__main__":
	all = RSyncRunner.list_rsync()
	for i in all:
		print(model_to_dict(i))
	print(RSyncRunner.rm_rsync(1))
