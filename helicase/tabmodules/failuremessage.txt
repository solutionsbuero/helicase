Helicase backup finished with errors


Backup job "{{data.job.human}}" finished at {{data.timestring}} with errors.

{% for i in data.errors %}{% if i.dump_id %}
Database dump of "{{i.dump_id.dbname}}@{{i.dump_id.host}}" failed with return code {{i.return_code}}.

STDERR:
{{i.stderr}}

{% elif i.rsync_id %}
rsync command {{i.rsync_id.human}} failed with return code {{i.return_code}}.

STDERR:
{{i.stderr}}{% elif i.cmd_id %}
Misc command {{i.cmd_id.human}} failed with return code {{i.return_code}}.

STDERR:
{{i.stderr}}{% else %}
Something unexpected failed.

STDERR:
{{i.stderr}}
{% endif %}
{% endfor %}
