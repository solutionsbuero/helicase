import sys
import datetime
from dataclasses import dataclass
from typing import Optional, List
from crontab import CronTab, CronSlice, CronItem


@dataclass
class CronForm:
	job_id: int
	enabled: bool = False
	cmd: str = ""
	min_slice: str = "*"
	hou_slice: str = "*"
	dom_slice: str = "*"
	mon_slice: str = "*"
	dow_slice: str = "*"
	all_slices: str = ""

	def assemble_slices(self):
		self.all_slices = "{} {} {} {} {}".format(
			self.min_slice,
			self.hou_slice,
			self.dom_slice,
			self.mon_slice,
			self.dow_slice
		)


class CronManager:

	@staticmethod
	def obj_from_form(job_id, formdata: dict) -> CronForm:
		obj = CronForm(
			job_id=job_id,
			enabled=bool(formdata["enabled"]),
			cmd=CronManager.assemble_cmd(job_id=job_id),
			min_slice=formdata["min_slice"],
			hou_slice=formdata["hou_slice"],
			dom_slice=formdata["dom_slice"],
			mon_slice=formdata["mon_slice"],
			dow_slice=formdata["dow_slice"]
		)
		obj.assemble_slices()
		return obj

	@staticmethod
	def assemble_cmd(job_id: int) -> str:
		python = sys.executable
		return "{} -m helicase.evoke {}".format(python, job_id)

	@staticmethod
	def get_job_by_id(tab: CronTab, job_id: int) -> Optional[CronForm]:
		cmd = CronManager.assemble_cmd(job_id)
		iterator = tab.find_command(cmd)
		job = next(iterator, None)
		if job is None:
			return None
		obj = CronForm(
			job_id=job_id,
			enabled=job.enabled,
			cmd=cmd,
			min_slice=job.slices[0],
			hou_slice=job.slices[1],
			dom_slice=job.slices[2],
			mon_slice=job.slices[3],
			dow_slice=job.slices[4]
		)
		obj.assemble_slices()
		return obj

	@staticmethod
	def set_cronjob(job_id: int, formdata: dict) -> bool:
		crontab = CronTab(user=True)
		new = CronManager.obj_from_form(job_id, formdata)
		cmd = CronManager.assemble_cmd(job_id)
		iterator = crontab.find_command(cmd)
		job = next(iterator, None)
		if job is None:
			job = crontab.new(command=cmd)
		job.set_command(cmd=cmd)
		job.enable(new.enabled)
		job.set_comment("helicase job {}".format(job_id))
		job.setall(new.all_slices)
		try:
			if job.is_valid():
				crontab.write()
				return True
			return False
		except ValueError:
			return False

	@staticmethod
	def get_cronjob(job_id: int) -> (bool, CronForm):
		crontab = CronTab(user=True)
		has_job = True
		job = CronManager.get_job_by_id(crontab, job_id)
		if job is None:
			has_job = False
			job = CronForm(job_id=job_id)
		return has_job, job

	@staticmethod
	def rm_cronjob(job_id: int):
		crontab = CronTab(user=True)
		cmd = CronManager.assemble_cmd(job_id)
		iterator = crontab.find_command(cmd)
		for i in iterator:
			crontab.remove(i)
		crontab.write()

	@staticmethod
	def get_next_run(job_id: int) -> Optional[int]:
		crontab = CronTab(user=True)
		cmd = CronManager.assemble_cmd(job_id)
		iterator = crontab.find_command(cmd)
		job = next(iterator, None)
		if job is None:
			return None
		elif not job.enabled:
			return 0
		else:
			schedule = job.schedule(date_from=datetime.datetime.now())
			return int(schedule.get_next().timestamp())


if __name__ == "__main__":
	print(CronManager.get_next_run(1))