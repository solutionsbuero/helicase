import json
import yaml
from peewee import fn
from typing import List, Optional
from helicase.database.models import RunProtocol
from playhouse.shortcuts import model_to_dict
from helicase.database.connect import DBPath


class CmdDB:

	@staticmethod
	def list_cmd() -> list:
		cmd_file = DBPath.get_cmd_file()
		with cmd_file.open() as f:
			content = f.read()
		cmd_list = yaml.safe_load(content)
		if type(cmd_list) is not list:
			raise TypeError("cmd file should contain a list; check your yaml file.")
		for n, i in enumerate(cmd_list, start=0):
			if type(i) is not dict:
				raise TypeError("cmd entry should be a dict; check your yaml file.")
			i.update({"id": n})
		return cmd_list

	@staticmethod
	def get_cmd(cmdid: int) -> dict:
		return CmdDB.list_cmd()[cmdid]
