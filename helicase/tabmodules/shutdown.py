import subprocess


class ShutdownHandler:

	@staticmethod
	def unlock_shutdown(userpass: str) -> (int, str, str):
		subprocess.call("export HISTIGNORE='*sudo -S*'", shell=True)
		cmd = 'echo "{}" | sudo -S -- sh -c "echo \\"$USER ALL=(ALL) NOPASSWD: /sbin/shutdown\\" | sudo EDITOR=\\"tee -a\\" visudo" && sudo -k'.format(userpass)
		unlock_process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		stdout, stderr = unlock_process.communicate()
		rc = unlock_process.returncode
		return rc, stdout.decode(), stderr.decode()

	@staticmethod
	def unlock_shutdown_pkexec() -> (int, str, str):
		cmd = 'echo "$USER ALL=(ALL) NOPASSWD: /sbin/shutdown" | pkexec sudo EDITOR="tee -a" visudo && sudo -k'
		unlock_process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		stdout, stderr = unlock_process.communicate()
		rc = unlock_process.returncode
		return rc, stdout.decode(), stderr.decode()

	@staticmethod
	def shutdown_now():
		cmd = 'echo "" | sudo -S shutdown -h now'
		dump_process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		stdout, stderr = dump_process.communicate()
		rc = dump_process.returncode
		return rc, stdout.decode(), stderr.decode()

	@staticmethod
	def unpriv_shutdown():
		cmd = "shutdown -h +1"
		subprocess.call(cmd)
