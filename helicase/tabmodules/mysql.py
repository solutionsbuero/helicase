import json
import subprocess
from pathlib import Path
from peewee import fn
from typing import List, Optional
from helicase.database.models import MySQLDump, RunProtocol
from playhouse.shortcuts import model_to_dict


class DumpDB:

	@staticmethod
	def list_dumps() -> List[MySQLDump]:
		return MySQLDump.select().order_by(fn.Lower(MySQLDump.dbname).asc())

	@staticmethod
	def get_dump(dumpid: int) -> MySQLDump:
		return MySQLDump.select().where(MySQLDump.id == dumpid).get()

	@staticmethod
	def get_dump_json(dumpid: int) -> str:
		query = MySQLDump.select().where(MySQLDump.id == dumpid).get()
		return json.dumps(model_to_dict(query))

	@staticmethod
	def edit_dump(formdata: dict, dumpid: Optional[int] = None):
		if dumpid is None:
			MySQLDump.create(
				host=formdata['host'],
				port=int(formdata['port']),
				username=formdata['username'],
				password=formdata['passwd'],
				dbname=formdata['dbname'],
				dumppath=formdata['dumppath']
			)
		else:
			dump = MySQLDump.get(MySQLDump.id == dumpid)
			dump.host = formdata['host']
			dump.port = int(formdata['port'])
			dump.username = formdata['username']
			dump.password = formdata['passwd']
			dump.dbname = formdata['dbname']
			dump.dumppath = formdata['dumppath']
			dump.save()

	@staticmethod
	def rm_dump(dumpid: int) -> Optional[list]:
		dump = MySQLDump.get(MySQLDump.id == dumpid)
		in_jobs = [i.job_id.human for i in dump.jobsassoc]
		if len(in_jobs) > 0:
			return in_jobs
		protocol_entries = RunProtocol.select().where(RunProtocol.dump_id == dumpid)
		for j in protocol_entries:
			j.delete_instance()
		dump.delete_instance(recursive=True)

	@staticmethod
	def assemble_cmd(dumpdata: MySQLDump) -> (list, Path):
		cmd = [
			"mysqldump",
			"--host={}".format(dumpdata.host),
			"--port={}".format(dumpdata.port),
			"--user={}".format(dumpdata.username),
			"--password={}".format(dumpdata.password),
			dumpdata.dbname
		]
		dumpfile = Path(dumpdata.dumppath)
		return cmd, dumpfile

	@staticmethod
	def dump_db_sync(dumpid: int) -> (int, str, str):
		dumpdata = DumpDB.get_dump(dumpid=dumpid)
		cmd, dumpfile = DumpDB.assemble_cmd(dumpdata)
		subprocess.call("export HISTIGNORE='*mysqldump*'", shell=True)
		with dumpfile.open("w") as dfile:
			dump_process = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=dfile)
		stdout, stderr = dump_process.communicate()
		rc = dump_process.returncode
		return rc, stdout, stderr.decode()
