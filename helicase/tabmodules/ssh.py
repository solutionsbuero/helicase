import os
import pwd
import subprocess
from pathlib import Path
from Crypto.PublicKey import RSA


class KeyManager:

	@staticmethod
	def get_key_path() -> (Path, Path):
		look_in = Path(os.environ['HOME']).resolve() / '.ssh'
		priv_key = look_in / "id_rsa"
		pub_key = look_in / "id_rsa.pub"
		return priv_key, pub_key

	@staticmethod
	def check_keys() -> (bool, bool):
		priv_key, pub_key = KeyManager.get_key_path()
		return priv_key.is_file(), pub_key.is_file()

	@staticmethod
	def check_keys_dict() -> dict:
		private_exists, public_exists = KeyManager.check_keys()
		return {"private_exists": private_exists, "public_exists": public_exists}

	@staticmethod
	def create_private_key():
		priv_key, pub_key = KeyManager.get_key_path()
		key = RSA.generate(2048)
		with priv_key.open("wb") as f:
			f.write(key.exportKey('PEM'))
		os.chmod(str(priv_key), 0o600)

	@staticmethod
	def create_public_key():
		priv_key, pub_key = KeyManager.get_key_path()
		with priv_key.open() as f:
			data = f.read()
		key = RSA.importKey(data)
		publickey = key.publickey()
		with pub_key.open("wb") as f:
			f.write(publickey.exportKey('OpenSSH'))

	@staticmethod
	def assemble_copy_cmd(username: str, hostname: str, port: int, password: str) -> str:
		cmd = "sshpass -p {} ssh-copy-id {}@{} -p {}".format(
			password,
			username,
			hostname,
			port
		)
		return cmd

	@staticmethod
	def copy_id(username: str, hostname: str, port: int, password: str) -> (int, str, str):
		subprocess.call("export HISTIGNORE='*sshpass -p*'", shell=True)
		cmd = KeyManager.assemble_copy_cmd(username, hostname, port, password)
		copy_process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout, stderr = copy_process.communicate()
		rc = copy_process.returncode
		return rc, stdout.decode(), stderr.decode()


if __name__ == "__main__":
	print(pwd.getpwuid(os.getuid()).pw_name)
	print(KeyManager.check_keys_dict())
