import subprocess
import multiprocessing
import datetime
from typing import Optional
from peewee import Model
from helicase.database.models import Jobs, RunQueue, RunProtocol, JobCmdAssoc, JobRsyncAssoc, JobMySQLAssoc
from helicase.tabmodules.jobs import JobManager
from helicase.tabmodules.mysql import DumpDB
from helicase.tabmodules.rsync import RSyncRunner
from helicase.tabmodules.smtp import EMailDB
from helicase.tabmodules.command import CmdDB
from playhouse.shortcuts import model_to_dict


class JobRunner:

	@staticmethod
	def check_running() -> Optional[int]:
		running = Jobs.select().where(Jobs.running == 1)
		if len(running):
			return running.get().id

	@staticmethod
	def check_runqueue(jobid: int):
		check = RunQueue.select().where(RunQueue.job_id == jobid)
		return bool(len(check))

	@staticmethod
	def check_and_run(jobid: int) -> (int, str):
		check_running = JobRunner.check_running()
		if check_running is not None:
			if check_running == jobid:
				return 2, "Job already running"
			elif JobRunner.check_runqueue(jobid):
				return 3, "Job already in queue"
			else:
				entry = RunQueue.create(job_id=jobid)
				entry.save()
				return 1, "Job added to runqueue"
		else:
			job = Jobs.select().where(Jobs.id == jobid).get()
			job.running = True
			job.save()
			job_process = multiprocessing.Process(target=JobRunner.run_job, args=(jobid,))
			job_process.start()
			return 0, "Job started"

	@staticmethod
	def run_job(jobid: int):

		# Clear old protocol
		to_clear = RunProtocol.select().where(RunProtocol.job_id == jobid)
		for i in to_clear:
			i.delete_instance()

		# Execute subtasks
		this_job = JobManager.get_job(jobid)
		for i in this_job.cmdassoc:
			if i.phase == "before_sql":
				JobRunner.run_task(task=i, jobid=jobid)
		for i in this_job.mysqlassoc:
			JobRunner.run_task(task=i, jobid=jobid)
		for i in this_job.cmdassoc:
			if i.phase == "before_rsync":
				JobRunner.run_task(task=i, jobid=jobid)
		for i in this_job.rsyncassoc:
			JobRunner.run_task(task=i, jobid=1)
		for i in this_job.cmdassoc:
			if i.phase == "before_shutdown":
				JobRunner.run_task(task=i, jobid=jobid)

		# Write results and finish job
		this_job.running = False
		now_stamp = datetime.datetime.now().timestamp()
		complete_success = JobRunner.check_all_success(jobid=jobid)
		this_job.last_run_successful = complete_success
		this_job.last_run = now_stamp
		this_job.save()
		if complete_success:
			this_job.last_success = now_stamp
			this_job.save()
			if this_job.smtp is not None and this_job.smtp_send_success:
				EMailDB.send_success(jobid=jobid)
		elif this_job.smtp is not None:
			EMailDB.send_failure(jobid=jobid)

		if this_job.shutdown:
			# Clear run queue on shutdown
			to_clear = RunQueue.select()
			for i in to_clear:
				i.delete_instance()
				# Shutdown in 1min
			output = subprocess.run("shutdown -h +1", shell=True, capture_output=True)

		else:
			# Process run queue
			queue_job = RunQueue.select().order_by(RunQueue.id)
			if bool(len(queue_job)):
				next_job = queue_job.get()
				next_id = next_job.id
				next_job.delete_instance()
				JobRunner.run_job(next_id)

	@staticmethod
	def run_task(task: Model, jobid: int):
		protocol = RunProtocol.create(job_id=jobid)
		protocol.save()

		if type(task) == JobCmdAssoc:
			protocol.cmd_id = task.cmd_id
			protocol.save()
			cmd_dict = CmdDB.get_cmd(task.cmd_id)
			cmd = str(cmd_dict.get("cmd"))
			taskp = subprocess.run(cmd, capture_output=True, shell=True)
			protocol.return_code = taskp.returncode
			protocol.stdout = taskp.stdout
			protocol.stderr = taskp.stderr
			protocol.save()

		elif type(task) == JobMySQLAssoc:
			protocol.dump_id = task.mysql_id.id
			protocol.save()
			protocol.return_code, protocol.stdout, protocol.stderr = DumpDB.dump_db_sync(task.mysql_id.id)
			protocol.save()

		elif type(task) == JobRsyncAssoc:
			protocol.rsync_id = task.rsync_id.id
			protocol.save()
			protocol.return_code, protocol.stdout, protocol.stderr = RSyncRunner.run_rsync_sync(task.rsync_id.id)
			protocol.save()

		else:
			cmd = "(exit 1); echo Unknown task model >&2"
			taskp = subprocess.run(cmd, capture_output=True, shell=True)
			protocol.return_code = taskp.returncode
			protocol.stdout = taskp.stdout
			protocol.stderr = taskp.stderr
			protocol.save()

	@staticmethod
	def check_all_success(jobid: int) -> bool:
		entries = RunProtocol.select().where((RunProtocol.job_id == jobid) & (RunProtocol.return_code > 0))
		return not bool(len(entries))
