import json
import smtplib
import datetime
from pathlib import Path
from typing import Union
from jinja2 import Template
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from peewee import fn
from typing import List, Optional
from helicase.database.models import EMail, Jobs
from playhouse.shortcuts import model_to_dict
from helicase.tabmodules.jobs import JobManager


class EMailDB:

	@staticmethod
	def list_mail() -> List[EMail]:
		return EMail.select().order_by(fn.Lower(EMail.from_addr).asc())

	@staticmethod
	def get_mail(mailid: int) -> EMail:
		return EMail.select().where(EMail.id == mailid).get()

	@staticmethod
	def get_mail_json(mailid: int) -> str:
		query = EMail.select().where(EMail.id == mailid).get()
		return json.dumps(model_to_dict(query))

	@staticmethod
	def edit_mail(formdata: dict, mailid: Optional[int] = None):
		if mailid is None:
			EMail.create(
				host=formdata['host'],
				ssl=bool(formdata['ssl']),
				port=int(formdata['port']),
				username=formdata['username'],
				password=formdata['passwd'],
				from_addr=formdata['from_addr']
			)
		else:
			mail = EMail.get(EMail.id == mailid)
			mail.host = formdata['host']
			mail.ssl = bool(formdata['ssl'])
			mail.port = int(formdata['port'])
			mail.username = formdata['username']
			mail.password = formdata['passwd']
			mail.from_addr = formdata['from_addr']
			mail.save()

	@staticmethod
	def rm_mail(mailid: int) -> Optional[list]:
		mail = EMail.get(EMail.id == mailid)
		jobs = Jobs.select().where(Jobs.smtp == mailid)
		in_jobs = [i.human for i in jobs]
		if len(in_jobs) > 0:
			return in_jobs
		mail.delete_instance(recursive=True)

	@staticmethod
	def connect(account: EMail) -> Union[smtplib.SMTP, smtplib.SMTP_SSL]:
		if account.ssl:
			mailserver = smtplib.SMTP_SSL(host=account.host, port=account.port)
		else:
			mailserver = smtplib.SMTP(host=account.host, port=account.port)
		mailserver.login(account.username, account.password)
		return mailserver

	@staticmethod
	def compose(account: EMail, to: list, subject: str, message: str) -> MIMEMultipart:
		msg = MIMEMultipart()
		msg['From'] = account.from_addr
		msg['To'] = ", ".join(to)
		msg['subject'] = subject
		msg.attach(MIMEText(message, 'plain'))
		return msg

	@staticmethod
	def send(account_id: int, to: list, subject: str, message: str):
		account = EMail.select().where(EMail.id == account_id).get()
		mailserver = EMailDB.connect(account=account)
		full_message = EMailDB.compose(account=account, to=to, subject=subject, message=message)
		mailserver.send_message(full_message)

	@staticmethod
	def test_message(account_id: int, to: str):
		list_to = [to]
		subject = "Helicase Test Message"
		testfile = Path(__file__).resolve().parent / "testmessage.txt"
		with testfile.open() as f:
			message = f.read()
		EMailDB.send(account_id=account_id, to=list_to, subject=subject, message=message)

	@staticmethod
	def send_success(jobid: int):
		job = JobManager.get_job(jobid)
		dt = datetime.datetime.fromtimestamp(job.last_run).strftime("%d.%m.%Y - %H:%M:%S")
		account_id = job.smtp
		send_to = [i.strip() for i in job.smtp_to.split(";")]
		subject = "Backup successful"
		tmplpath = Path(__file__).resolve().parent / "successmessage.txt"
		with tmplpath.open() as f:
			tmpl = Template(f.read())
		message = tmpl.render(data={
			"job": job,
			"timestring": dt
		})
		EMailDB.send(account_id=account_id, to=send_to, subject=subject, message=message)

	@staticmethod
	def send_failure(jobid: int):
		job = JobManager.get_job(jobid)
		dt = datetime.datetime.fromtimestamp(job.last_run).strftime("%d.%m.%Y - %H:%M:%S")
		account_id = job.smtp
		send_to = [i.strip() for i in job.smtp_to.split(";")]
		subject = "Backup finished with errors"
		tmplpath = Path(__file__).resolve().parent / "failuremessage.txt"
		with tmplpath.open() as f:
			tmpl = Template(f.read())
		message = tmpl.render(data={
			"job": job,
			"timestring": dt,
			"errors": JobManager.get_error_protocol(jobid)
		})
		EMailDB.send(account_id=account_id, to=send_to, subject=subject, message=message)
