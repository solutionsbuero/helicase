import json
import hashlib
import secrets
import datetime
from typing import List, Optional
from helicase.database.models import Users, Sessions
from helicase.database.config import ConfigDB
from peewee import fn
from playhouse.shortcuts import model_to_dict


class UserDB:

	@staticmethod
	def list_users() -> List[Users]:
		return Users.select().order_by(fn.Lower(Users.username).asc())

	@staticmethod
	def get_user_json(userid: int) -> str:
		query = Users.select().where(Users.id == userid).get()
		return json.dumps(model_to_dict(query))

	@staticmethod
	def check_unique_user(username: str) -> bool:
		query = Users.select().where(Users.username == username)
		return not bool(len(query))

	@staticmethod
	def check_unique_token(token: str) -> bool:
		query = Sessions.select().where(Sessions.token == token)
		return not bool(len(query))

	@staticmethod
	def edit_user(formdata: dict, userid: Optional[int] = None):
		salt = secrets.token_urlsafe(8)
		password_hash = hashlib.sha256((formdata['passwd'] + salt).encode()).hexdigest()
		if userid is None:
			Users.create(
				username=formdata['username'],
				password_hash=password_hash,
				salt=salt
			)
		else:
			user = Users.get(Users.id == userid)
			user.username = formdata['username']
			user.password_hash = password_hash
			user.salt = salt
			user.save()

	@staticmethod
	def rm_user(userid: int):
		user = Users.get(Users.id == userid)
		user.delete_instance(recursive=True)

	@staticmethod
	def check_token(token: Optional[str] = None) -> bool:
		if token is None:
			return False
		query = Sessions.select().where(Sessions.token == token)
		if len(query) == 0:
			return False
		probe = query.get()
		return probe.expires.timestamp() >= datetime.datetime.now().timestamp()

	@staticmethod
	def check_credentials(username: str, password: str) -> (Optional[str], Optional[int]):
		query = Users.select().where(Users.username == username)
		if len(query) == 0:
			return None, None
		res = query.get()
		compare_to = hashlib.sha256((password + res.salt).encode()).hexdigest()
		if not compare_to == res.password_hash:
			return None, None
		return UserDB.create_session(res.id)

	@staticmethod
	def create_session(usrid: int) -> (str, int):
		token_unique = False
		token = ""
		while not token_unique:
			token = secrets.token_urlsafe(32)
			token_unique = UserDB.check_unique_token(token)
		serverconf = ConfigDB.get_subgroup("server")
		max_age = serverconf['session_duration']
		expires = datetime.datetime.now().timestamp() + max_age
		Sessions.create(
			userid=usrid,
			token=token,
			expires=expires
		)
		return token, max_age
