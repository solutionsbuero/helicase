import json
from dataclasses import dataclass
from typing import Optional, List
from playhouse.shortcuts import fn, model_to_dict
from helicase.database.models import Jobs
from helicase.database.models import MySQLDump, JobMySQLAssoc, RSync, JobRsyncAssoc, JobCmdAssoc, RunProtocol
from helicase.tabmodules.cron import CronManager
from helicase.tabmodules.command import CmdDB


@dataclass
class ExtendedCmd:
	id: int
	human: str
	cmd: str


class JobManager:

	CMD_PHASES = [
		'before_sql',
		'before_rsync',
		'before_shutdown'
	]

	@staticmethod
	def list_jobs() -> List[Jobs]:
		jobs = Jobs.select().order_by(fn.Lower(Jobs.human).asc())
		jobs_out = []
		for i in jobs:
			i.next_cron = CronManager.get_next_run(i.id)
			jobs_out.append(i)
		return jobs_out

	@staticmethod
	def get_job(jobid: int) -> Jobs:
		job = Jobs.select().where(Jobs.id == jobid).get()
		job.mysqlassoc = job.mysqlassoc.order_by(JobMySQLAssoc.orderno.asc())
		job.rsyncassoc = job.rsyncassoc.order_by(JobRsyncAssoc.orderno.asc())
		job.cmdassoc = job.cmdassoc.order_by(JobCmdAssoc.orderno.asc())
		job.protocol = job.protocol.order_by(RunProtocol.id.asc())
		return job

	@staticmethod
	def get_job_json(jobid: int) -> str:
		query = Jobs.select().where(Jobs.id == jobid).get()
		return json.dumps(model_to_dict(query))

	@staticmethod
	def edit_job(formdata: dict, jobid: Optional[int] = None):
		if formdata["smtp"] == "null" or formdata["smtp"] == "":
			conv_smtp = None
		else:
			conv_smtp = int(formdata['smtp'])
		if jobid is None:
			Jobs.create(
				human=formdata['human'],
				smtp=conv_smtp,
				smtp_send_success=bool(formdata['smtp_send_success']),
				smtp_to=formdata['smtp_to'],
				shutdown=bool(formdata['shutdown'])
			)
		else:
			job = Jobs.get(Jobs.id == jobid)
			job.human = formdata['human']
			job.smtp = conv_smtp
			job.smtp_send_success = bool(formdata['smtp_send_success'])
			job.smtp_to = formdata['smtp_to']
			job.shutdown = bool(formdata['shutdown'])
			job.save()

	@staticmethod
	def rm_job(jobid: int):
		job = Jobs.get(Jobs.id == jobid)
		job.delete_instance(recursive=True)

	@staticmethod
	def get_filtered_mysql(jobid: int) -> List[MySQLDump]:
		job = Jobs.select().where(Jobs.id == jobid).get()
		already_in_use = []
		for i in job.mysqlassoc:
			already_in_use.append(i.mysql_id)
		filtered_dumps = MySQLDump.select().where(~(MySQLDump.id << already_in_use)).order_by(fn.Lower(MySQLDump.dbname).asc())
		return filtered_dumps

	@staticmethod
	def add_mysql_assoc(jobid: int, dumpid: int):
		next_orderno = int(JobMySQLAssoc.select(fn.MAX(JobMySQLAssoc.orderno)).where(JobMySQLAssoc.job_id == jobid).scalar() or 0) + 1
		JobMySQLAssoc.create(
			job_id=jobid,
			mysql_id=dumpid,
			orderno=next_orderno
		)

	@staticmethod
	def rm_mysql_assoc(associd: int):
		assoc = JobMySQLAssoc.get(JobMySQLAssoc.id == associd)
		assoc.delete_instance()

	@staticmethod
	def mv_mysql_assoc(jobid: int, associd: int, direction: int):
		this_entry = JobMySQLAssoc.get(JobMySQLAssoc.id == associd)
		current_orderno = this_entry.orderno
		if direction > 0:
			next_entry = JobMySQLAssoc.select().where((JobMySQLAssoc.job_id == jobid) & (JobMySQLAssoc.orderno < current_orderno))
		else:
			next_entry = JobMySQLAssoc.select().where((JobMySQLAssoc.job_id == jobid) & (JobMySQLAssoc.orderno > current_orderno))
		if len(next_entry) > 0:
			next_entry = next_entry.get()
			this_entry.orderno = next_entry.orderno
			next_entry.orderno = current_orderno
			this_entry.save()
			next_entry.save()

	@staticmethod
	def get_filtered_rsync(jobid: int) -> List[RSync]:
		job = Jobs.select().where(Jobs.id == jobid).get()
		already_in_use = []
		for i in job.rsyncassoc:
			already_in_use.append(i.rsync_id)
		filtered_cmd = RSync.select().where(~(RSync.id << already_in_use)).order_by(fn.Lower(RSync.human).asc())
		return filtered_cmd

	@staticmethod
	def add_rsync_assoc(jobid: int, cmdid: int):
		next_orderno = int(JobRsyncAssoc.select(fn.MAX(JobRsyncAssoc.orderno)).where(JobRsyncAssoc.job_id == jobid).scalar() or 0) + 1
		JobRsyncAssoc.create(
			job_id=jobid,
			rsync_id=cmdid,
			orderno=next_orderno
		)

	@staticmethod
	def rm_rsync_assoc(associd: int):
		assoc = JobRsyncAssoc.get(JobRsyncAssoc.id == associd)
		assoc.delete_instance()

	@staticmethod
	def mv_rsync_assoc(jobid: int, associd: int, direction: int):
		this_entry = JobRsyncAssoc.get(JobRsyncAssoc.id == associd)
		current_orderno = this_entry.orderno
		if direction > 0:
			next_entry = JobRsyncAssoc.select().where((JobRsyncAssoc.job_id == jobid) & (JobRsyncAssoc.orderno < current_orderno))
		else:
			next_entry = JobRsyncAssoc.select().where((JobRsyncAssoc.job_id == jobid) & (JobRsyncAssoc.orderno > current_orderno))
		if len(next_entry) > 0:
			next_entry = next_entry.get()
			this_entry.orderno = next_entry.orderno
			next_entry.orderno = current_orderno
			this_entry.save()
			next_entry.save()

	@staticmethod
	def get_filtered_cmd(jobid: int, phase: str) -> list:
		job = Jobs.select().where(Jobs.id == jobid).get()
		already_in_use = []
		for i in job.cmdassoc:
			if i.phase == phase:
				already_in_use.append(i.cmd_id)
		cmd_list = CmdDB.list_cmd()
		return [i for i in cmd_list if i["id"] not in already_in_use]

	@staticmethod
	def add_cmd_assoc(jobid: int, cmdid: int, phase: str):
		next_orderno = int(JobCmdAssoc.select(fn.MAX(JobCmdAssoc.orderno)).where((JobCmdAssoc.job_id == jobid) & (JobCmdAssoc.phase == phase)).scalar() or 0) + 1
		JobCmdAssoc.create(
			job_id=jobid,
			cmd_id=cmdid,
			orderno=next_orderno,
			phase=phase
		)

	@staticmethod
	def rm_cmd_assoc(associd: int):
		assoc = JobCmdAssoc.get(JobCmdAssoc.id == associd)
		assoc.delete_instance()

	@staticmethod
	def mv_cmd_assoc(jobid: int, associd: int, direction: int, phase: str):
		this_entry = JobCmdAssoc.get(JobCmdAssoc.id == associd)
		current_orderno = this_entry.orderno
		if direction > 0:
			next_entry = JobCmdAssoc.select().where(
				(JobCmdAssoc.job_id == jobid) &
				(JobCmdAssoc.orderno < current_orderno) &
				(JobCmdAssoc.phase == phase)
			)
		else:
			next_entry = JobCmdAssoc.select().where(
				(JobCmdAssoc.job_id == jobid) &
				(JobCmdAssoc.orderno > current_orderno) &
				(JobCmdAssoc.phase == phase)
			)
		if len(next_entry) > 0:
			next_entry = next_entry.get()
			this_entry.orderno = next_entry.orderno
			next_entry.orderno = current_orderno
			this_entry.save()
			next_entry.save()

	@staticmethod
	def get_error_protocol(jobid: int) -> list:
		errors = RunProtocol.select().where((RunProtocol.return_code > 0) & (RunProtocol.job_id == jobid))
		error_list = list()
		for i in errors:
			if i.cmd_id is not None:
				add_info = CmdDB.get_cmd(i)
				j = ExtendedCmd(id=i, human=add_info["human"], cmd=add_info["cmd"])
				error_list.append(j)
			else:
				error_list.append(i)
		return error_list


if __name__ == "__main__":
	job = JobManager.get_job(1)
	print("Job:", model_to_dict(job))
	for i in job.protocol:
		print("P entry: ", model_to_dict(i))
