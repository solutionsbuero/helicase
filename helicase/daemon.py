from helicase.sharedobjects import SharedObjects, InterfaceStyle
from helicase.server.server import HelicaseServer

shared_obj = SharedObjects(
	interface_style=InterfaceStyle.DAEMON,
	main_window=None
)

srv = HelicaseServer(shared_obj=shared_obj)
srv.start()
