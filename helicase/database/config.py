from helicase.database.models import Config


class ConfigDB:

	@staticmethod
	def get_subgroup(subgroup: str) -> dict:
		conf_list = {}
		query = Config.select().where(Config.subgroup == subgroup)
		for i in query:
			conf_list.update({i.conf_key: ConfigDB.type_converstion(i)})
		return conf_list

	@staticmethod
	def type_converstion(entry: Config):
		if entry.datatype == "int":
			return int(entry.conf_val)
		elif entry.datatype == "bool":
			return bool(entry.conf_val)
		else:
			return str(entry.conf_val)
