from peewee import SqliteDatabase, Model, CharField, IntegerField, BooleanField, ForeignKeyField, TextField, TimestampField, AutoField
from helicase.database.connect import DBPath
from playhouse.signals import pre_save
from playhouse.shortcuts import fn


db = SqliteDatabase(DBPath.copy_and_open())


class BaseModel(Model):
	class Meta:
		database = db


class Users(BaseModel):
	username = TextField()
	password_hash = TextField()
	salt = TextField()

	class Meta:
		table_name = "accounts"


class Sessions(BaseModel):
	usrid = ForeignKeyField(Users, column_name="usrid", backref='sessions')
	token = TextField()
	expires = TimestampField(resolution=0)

	class Meta:
		table_name = "sessions"


class Config(BaseModel):
	conf_key = TextField()
	conf_val = TextField()
	datatype = TextField()
	subgroup = TextField()

	class Meta:
		table_name = "config"


class EMail(BaseModel):
	host = CharField()
	ssl = BooleanField(default=True)
	port = IntegerField(default=465)
	username = CharField()
	password = CharField()
	from_addr = CharField()


class MySQLDump(BaseModel):
	host = CharField(default="localhost")
	port = IntegerField(default=3306)
	username = CharField()
	password = CharField()
	dbname = CharField()
	dumppath = CharField(max_length=500)


class RSync(BaseModel):
	human = CharField()
	flags = CharField()
	source = CharField()
	destination = CharField()


class Jobs(BaseModel):
	human = CharField()
	running = BooleanField(default=False)
	last_run_successful = BooleanField(default=False)
	last_run = IntegerField(default=None, null=True)
	last_success = IntegerField(default=None, null=True)
	smtp = ForeignKeyField(EMail, backref='jobs', null=True)
	smtp_send_success = BooleanField(default=False)
	smtp_to = TextField(default="")
	shutdown = BooleanField(default=False)


class JobMySQLAssoc(BaseModel):
	job_id = ForeignKeyField(Jobs, backref='mysqlassoc', related_name='mysqlassoc')
	mysql_id = ForeignKeyField(MySQLDump, backref='jobsassoc', related_name='jobsassoc')
	orderno = IntegerField(default=1)

	@property
	def mysqlassoc_ordered(self):
		return self.mysqlassoc.order_by(JobMySQLAssoc.orderno.asc())


class JobRsyncAssoc(BaseModel):
	job_id = ForeignKeyField(Jobs, backref='rsyncassoc', related_name='rsyncassoc')
	rsync_id = ForeignKeyField(RSync, backref='jobsassoc', related_name='jobsassoc')
	orderno = IntegerField(default=1)


class JobCmdAssoc(BaseModel):
	job_id = ForeignKeyField(Jobs, backref='cmdassoc', related_name='cmdassoc')
	cmd_id = IntegerField()
	orderno = IntegerField(default=1)
	phase = CharField()


class RunQueue(BaseModel):
	job_id = ForeignKeyField(Jobs, backref='job', related_name='job')


class RunProtocol(BaseModel):
	job_id = ForeignKeyField(Jobs, backref='protocol', related_name='protocol')
	rsync_id = ForeignKeyField(RSync, backref='protocol', null=True, default=None)
	cmd_id = IntegerField(null=True, default=None)
	dump_id = ForeignKeyField(MySQLDump, backref='protocol', null=True, default=None)
	return_code = IntegerField(default=None, null=True)
	stdout = TextField(default=None, null=True)
	stderr = TextField(default=None, null=True)


class BruteForceLog(BaseModel):
	addr = CharField()
	stamp = IntegerField()


db.connect()
db.create_tables([EMail, MySQLDump, RSync, Jobs, JobMySQLAssoc, JobRsyncAssoc, JobCmdAssoc, RunQueue, RunProtocol, BruteForceLog])
