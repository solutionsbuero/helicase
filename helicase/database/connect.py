import os
import shutil
from pathlib import Path


class DBPath:

	# Cross-plattform config dir to save sqlite db and other user-editable assets
	@staticmethod
	def get_configpath() -> Path:
		# Config path for Windows, something like C:\User\username\Appdata\Roaming
		if 'APPDATA' in os.environ:
			confighome = Path(os.environ['APPDATA']).resolve()
		# Config path for macOS
		elif 'XDG_CONFIG_HOME' in os.environ:
			confighome = Path(os.environ['XDG_CONFIG_HOME']).resolve()
		# Config path for Linux, /home/username/.config
		else:
			confighome = Path(os.environ['HOME']).resolve() / '.config'
		configpath = confighome / 'helicase'
		return configpath

	# Copy initialization file if db does not exist, open db
	@staticmethod
	def copy_and_open(force_init: bool = False) -> Path:
		configpath = DBPath.get_configpath()
		dbpath = configpath / "config.db"
		if force_init and dbpath.is_file():
			dbpath.unlink()
		if not dbpath.is_file():
			print("No config db found (or init forced), initializing...")
			configpath.mkdir(exist_ok=True)
			initpath = Path(__file__).resolve().parent / "init_config.db"
			shutil.copyfile(initpath, dbpath)
			os.chmod(str(dbpath), 0o600)
		return dbpath

	@staticmethod
	def get_cmd_file() -> Path:
		cmd_file = Path(__file__).resolve().parent / "cmd.yaml"
		if not cmd_file.is_file():
			raise FileNotFoundError
		return cmd_file
