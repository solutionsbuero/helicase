import json
from typing import Optional
from fastapi import APIRouter, Request, Response
from fastapi.responses import JSONResponse
from helicase.server.decorators import login_required
from helicase.tabmodules.cron import CronManager


def cron_router(guibypass: bool) -> APIRouter:

	router = APIRouter()

	@router.post("/api/cron/{jobid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def edit_job(request: Request, jobid: Optional[int] = None):
		fdata = dict(await request.form())
		vaild = CronManager.set_cronjob(job_id=jobid, formdata=fdata)
		if vaild:
			return Response(
				content=json.dumps({"state": "ok", "jobid": jobid}),
				status_code=200,
				media_type="application/json"
			)
		else:
			return Response(
				content=json.dumps({"error": "Cron job invalid"}),
				status_code=512,
				media_type="application/json"
			)

	@router.delete("/api/cron/{jobid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def rm_job(request: Request, jobid: int):
		CronManager.rm_cronjob(jobid)
		return Response(
			content=json.dumps({"state": "ok", "jobid": jobid}),
			status_code=200,
			media_type="application/json"
		)

	return router