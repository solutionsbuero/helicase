from fastapi import APIRouter, Request, Response
from helicase.server.decorators import login_required
from helicase.server.partfactory import PartFactory
from helicase.sharedobjects import SharedObjects


def partial_router(guibypass: bool, shared_obj: SharedObjects) -> APIRouter:

	partfactory = PartFactory(shared_obj=shared_obj, gui=guibypass)
	router = APIRouter()

	@router.post("/part/usertable")
	@login_required(bypass=guibypass)
	async def partial_usertable(request: Request):
		return partfactory.render_usertable(req=request)

	@router.post("/part/mailtable")
	@login_required(bypass=guibypass)
	async def partial_mailtable(request: Request):
		return partfactory.render_mailtable(req=request)

	@router.post("/part/maildrop")
	@login_required(bypass=guibypass)
	async def partial_maildrop(request: Request):
		return partfactory.render_maildrop(req=request)

	@router.post("/part/dumptable")
	@login_required(bypass=guibypass)
	async def partial_dumptable(request: Request):
		return partfactory.render_dumptable(req=request)

	@router.post("/part/dumpdrop")
	@login_required(bypass=guibypass)
	async def partial_dumptable(request: Request):
		return partfactory.render_dumpdrop(req=request)

	@router.post("/part/own_keys")
	@login_required(bypass=guibypass)
	async def partial_own_keys(request: Request):
		return partfactory.render_own_keys(req=request)

	@router.post("/part/rsynclist")
	@login_required(bypass=guibypass)
	async def partial_rsynclist(request: Request):
		return partfactory.render_rsyncls(req=request)

	@router.post("/part/cmdlist")
	@login_required(bypass=guibypass)
	async def partial_cmdlist(request: Request):
		return partfactory.render_cmdlist(req=request)

	@router.post("/part/joblist")
	@login_required(bypass=guibypass)
	async def partial_joblist(request: Request):
		return partfactory.render_joblist(req=request)

	@router.post("/part/matchlist/{jobid}")
	@login_required(bypass=guibypass)
	async def partial_matchlist(request: Request, jobid: int):
		return partfactory.render_matchlist(req=request, jobid=jobid)

	@router.post("/part/cron/{jobid}")
	@login_required(bypass=guibypass)
	async def partial_cronform(request: Request, jobid: int):
		return partfactory.render_cronform(req=request, jobid=jobid)

	@router.post("/part/statelist")
	@login_required(bypass=guibypass)
	async def partial_statelist(request: Request):
		return partfactory.render_statelist(req=request)

	return router
