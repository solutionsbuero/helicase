import json
from typing import Optional
from fastapi import APIRouter, Request, Response
from fastapi.responses import JSONResponse
from helicase.server.decorators import login_required, check_job_running
from helicase.tabmodules.mysql import DumpDB


def dump_router(guibypass: bool) -> APIRouter:

	router = APIRouter()

	@router.get("/api/dump/{dumpid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def get_mail_json(request: Request, dumpid: int):
		return Response(
			content=DumpDB.get_dump_json(dumpid),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/dump", response_class=JSONResponse)
	@router.post("/api/dump/{dumpid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	@check_job_running
	async def edit_dump(request: Request, dumpid: Optional[int] = None):
		fdata = dict(await request.form())
		DumpDB.edit_dump(formdata=fdata, dumpid=dumpid)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.delete("/api/dump/{dumpid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	@check_job_running
	async def rm_dump(request: Request, dumpid: int):
		in_use = DumpDB.rm_dump(dumpid=dumpid)
		if in_use is not None:
			in_use = ", ".join(in_use)
			return Response(
				content=json.dumps({"error": "Still in use by jobs: {}".format(in_use)}),
				status_code=513,
				media_type="application/json"
			)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/testdump", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def test_dump(request: Request):
		fdata = dict(await request.form())
		rc, stdout, stderr = DumpDB.dump_db_sync(int(fdata["dumpid"]))
		if rc == 0:
			return Response(
				content=json.dumps({"state": "ok"}),
				status_code=200,
				media_type="application/json"
			)
		else:
			return Response(
				content=json.dumps({"rc": rc, "stdout": stdout, "stderr": stderr}),
				status_code=512,
				media_type="application/json"
			)

	return router
