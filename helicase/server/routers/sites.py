import json
from fastapi import APIRouter, Request, Response
from fastapi.responses import JSONResponse
from helicase.server.decorators import login_required
from helicase.server.tabfactory import TabFactory
from helicase.sharedobjects import SharedObjects


def sites_router(guibypass: bool, shared_obj: SharedObjects) -> APIRouter:

	tabfactory = TabFactory(shared_obj=shared_obj, gui=guibypass)
	router = APIRouter()

	@router.get("/")
	async def homepage(request: Request):
		return shared_obj.jinja_templates.TemplateResponse("root.html", {"request": request})

	@router.get("/tabs/login", response_class=JSONResponse)
	async def login_page(request: Request):
		jsonresp = json.dumps([
			tabfactory.render_login(req=request),
			tabfactory.render_about(req=request)
		])
		return Response(
			content=jsonresp,
			status_code=200,
			media_type="application/json"
		)

	@router.get("/tabs/content", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def maincontent(request: Request):
		jsonresp = json.dumps([
			tabfactory.render_statelist(req=request),
			tabfactory.render_joblist(req=request),
			tabfactory.render_rsynclist(req=request),
			tabfactory.render_dumps(req=request),
			tabfactory.render_command(req=request),
			tabfactory.render_mail(req=request),
			tabfactory.render_ssh(req=request),
			tabfactory.render_userlist(req=request),
			# tabfactory.render_shutdown(req=request),
			tabfactory.render_about(req=request),
			tabfactory.render_logout()
		])
		return Response(
			content=jsonresp,
			status_code=200,
			media_type="application/json"
		)

	return router
