import json
from typing import Optional
from fastapi import APIRouter, Request, Response
from fastapi.responses import JSONResponse
from helicase.tabmodules.jobs import JobManager
from helicase.tabmodules.runjob import JobRunner
from helicase.tabmodules.cron import CronManager
from helicase.server.decorators import login_required, check_job_running


def job_router(guibypass: bool) -> APIRouter:

	router = APIRouter()

	@router.get("/api/job/{jobid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def get_job_json(request: Request, jobid: int):
		return Response(
			content=JobManager.get_job_json(jobid),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/job", response_class=JSONResponse)
	@router.post("/api/job/{jobid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	@check_job_running
	async def edit_job(request: Request, jobid: Optional[int] = None):
		fdata = dict(await request.form())
		JobManager.edit_job(formdata=fdata, jobid=jobid)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.delete("/api/job/{jobid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	@check_job_running
	async def rm_job(request: Request, jobid: int):
		CronManager.rm_cronjob(jobid)
		JobManager.rm_job(jobid=jobid)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/job/{jobid}/mysqlassoc/{dumpid}")
	@login_required(bypass=guibypass)
	@check_job_running
	async def add_mysql_assoc(request: Request, jobid: int, dumpid: int):
		JobManager.add_mysql_assoc(jobid=jobid, dumpid=dumpid)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.delete("/api/job/{jobid}/mysqlassoc/{associd}")
	@login_required(bypass=guibypass)
	@check_job_running
	async def rm_mysql_assoc(request: Request, jobid: int, associd: int):
		JobManager.rm_mysql_assoc(associd)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/job/{jobid}/mysqlassoc/{associd}/mv/{direction}")
	@login_required(bypass=guibypass)
	@check_job_running
	async def mv_mysql_assoc(request: Request, jobid: int, associd: int, direction: int):
		JobManager.mv_mysql_assoc(jobid, associd, direction)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/job/{jobid}/rsyncassoc/{cmdid}")
	@login_required(bypass=guibypass)
	@check_job_running
	async def add_rsync_assoc(request: Request, jobid: int, cmdid: int):
		JobManager.add_rsync_assoc(jobid=jobid, cmdid=cmdid)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.delete("/api/job/{jobid}/rsyncassoc/{associd}")
	@login_required(bypass=guibypass)
	@check_job_running
	async def rm_mysql_assoc(request: Request, jobid: int, associd: int):
		JobManager.rm_rsync_assoc(associd)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/job/{jobid}/rsyncassoc/{associd}/mv/{direction}")
	@login_required(bypass=guibypass)
	@check_job_running
	async def mv_rsync_assoc(request: Request, jobid: int, associd: int, direction: int):
		JobManager.mv_rsync_assoc(jobid, associd, direction)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/job/{jobid}/cmdassoc/{cmdid}/phase/{phase}")
	@login_required(bypass=guibypass)
	@check_job_running
	async def add_rsync_assoc(request: Request, jobid: int, cmdid: int, phase: str):
		JobManager.add_cmd_assoc(jobid=jobid, cmdid=cmdid, phase=phase)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.delete("/api/job/{jobid}/cmdassoc/{associd}")
	@login_required(bypass=guibypass)
	@check_job_running
	async def rm_mysql_assoc(request: Request, jobid: int, associd: int):
		JobManager.rm_cmd_assoc(associd)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/job/{jobid}/cmdassoc/{associd}/mv/{direction}/phase/{phase}")
	@login_required(bypass=guibypass)
	@check_job_running
	async def mv_cmd_assoc(request: Request, jobid: int, associd: int, direction: int, phase: str):
		JobManager.mv_cmd_assoc(jobid, associd, direction, phase)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/evoke/{jobid}")
	@login_required(bypass=guibypass)
	async def evoke_job(request: Request, jobid: int):
		status, feedback = JobRunner.check_and_run(jobid)
		if status == 0:
			return Response(
				content=json.dumps({"state": feedback}),
				status_code=200,
				media_type="application/json"
			)
		elif status == 1:
			return Response(
				content=json.dumps({"state": feedback}),
				status_code=202,
				media_type="application/json"
			)
		elif status in [2, 3]:
			return Response(
				content=json.dumps({"state": feedback}),
				status_code=409,
				media_type="application/json"
			)

	return router
