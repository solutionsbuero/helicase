import json
from typing import Optional
from fastapi import APIRouter, Request, Response
from fastapi.responses import JSONResponse
from helicase.tabmodules.smtp import EMailDB
from helicase.server.decorators import login_required, check_job_running


def mail_router(guibypass: bool) -> APIRouter:

	router = APIRouter()

	@router.get("/api/mail/{mailid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def get_mail_json(request: Request, mailid: int):
		return Response(
			content=EMailDB.get_mail_json(mailid),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/mail", response_class=JSONResponse)
	@router.post("/api/mail/{mailid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	@check_job_running
	async def edit_mail(request: Request, mailid: Optional[int] = None):
		fdata = dict(await request.form())
		EMailDB.edit_mail(formdata=fdata, mailid=mailid)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.delete("/api/mail/{mailid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	@check_job_running
	async def rm_mail(request: Request, mailid: int):
		in_use = EMailDB.rm_mail(mailid=mailid)
		if in_use is not None:
			in_use = ", ".join(in_use)
			return Response(
				content=json.dumps({"error": "Still in use by jobs: {}".format(in_use)}),
				status_code=513,
				media_type="application/json"
			)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/testmail", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def send_testmail(request: Request):
		fdata = dict(await request.form())
		EMailDB.test_message(
			account_id=int(fdata["mail_to_test"]),
			to=fdata["mail_test_receiver"]
		)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	return router
