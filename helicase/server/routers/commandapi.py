import json
from typing import Optional
from fastapi import APIRouter, Request, Response
from fastapi.responses import JSONResponse
from helicase.tabmodules.command import CmdDB
from helicase.server.decorators import login_required


def command_router(guibypass: bool) -> APIRouter:

	router = APIRouter()

	@router.get("/api/cmd/{cmdid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def get_cmd_json(request: Request, cmdid: int):
		return Response(
			content=CmdDB.get_cmd_json(cmdid),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/cmd", response_class=JSONResponse)
	@router.post("/api/cmd/{cmdid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def edit_cmd(request: Request, cmdid: Optional[int] = None):
		fdata = dict(await request.form())
		CmdDB.edit_cmd(formdata=fdata, cmdid=cmdid)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.delete("/api/cmd/{cmdid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def rm_cmd(request: Request, cmdid: int):
		in_use = CmdDB.rm_cmd(cmdid=cmdid)
		if in_use is not None:
			in_use = ", ".join(in_use)
			return Response(
				content=json.dumps({"error": "Still in use by jobs: {}".format(in_use)}),
				status_code=513,
				media_type="application/json"
			)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	return router