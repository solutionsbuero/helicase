import json
from fastapi import APIRouter, Request, Response
from helicase.server.decorators import login_required, check_job_running
from helicase.tabmodules.shutdown import ShutdownHandler


def shutdown_router(guibypass: bool) -> APIRouter:

	router = APIRouter()

	@router.post("/api/shutdown/unlock")
	@login_required(bypass=guibypass)
	async def unlock_shutdown(request: Request):
		# fdata = dict(await request.form())
		# rc, stdout, stderr = ShutdownHandler.unlock_shutdown(fdata["sudo_passwd"])
		rc, stdout, stderr = ShutdownHandler.unlock_shutdown_pkexec()
		if rc == 0:
			return Response(
				content=json.dumps({"status": "ok"}),
				status_code=200,
				media_type="application/json"
			)
		else:
			return Response(
				content=json.dumps({"rc": rc, "stderr": stderr}),
				status_code=512,
				media_type="application/json"
			)

	@router.get("/api/shutdown/test")
	@login_required(bypass=guibypass)
	async def test_shutdown(request: Request):
		rc, stdout, stderr = ShutdownHandler.shutdown_now()
		if rc == 0:
			return Response(
				content=json.dumps({"status": "ok"}),
				status_code=200,
				media_type="application/json"
			)
		else:
			return Response(
				content=json.dumps({"rc": rc, "stderr": stderr}),
				status_code=512,
				media_type="application/json"
			)

	return router
