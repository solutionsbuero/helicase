import json
from typing import Optional
from fastapi import APIRouter, Request, Response
from fastapi.responses import JSONResponse
from helicase.tabmodules.users import UserDB
from helicase.server.decorators import login_required


def users_router(guibypass: bool) -> APIRouter:

	router = APIRouter()

	@router.get("/api/user/{userid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def get_user_json(request: Request, userid: int):
		return Response(
			content=UserDB.get_user_json(userid),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/user", response_class=JSONResponse)
	@router.post("/api/user/{userid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def edit_user(request: Request, userid: Optional[int] = None):
		fdata = dict(await request.form())
		if (not UserDB.check_unique_user(fdata['username'])) and (userid is None):
			return Response(
				content=json.dumps({"error": "Username already exists"}),
				status_code=400,
				media_type="application/json"
			)
		if fdata['username'] == "":
			return Response(
				content=json.dumps({"error": "Username should not be empty"}),
				status_code=400,
				media_type="application/json"
			)
		if len(fdata['passwd']) < 8 or fdata['passwd'] != fdata['passwd2']:
			return Response(
				content=json.dumps({"error": "Password too short or missmatched"}),
				status_code=400,
				media_type="application/json"
			)
		UserDB.edit_user(formdata=fdata, userid=userid)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.delete("/api/user/{userid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def edit_user(request: Request, userid: int):
		UserDB.rm_user(userid=userid)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	return router
