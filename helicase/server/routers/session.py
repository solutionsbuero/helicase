import json
import asyncio
import datetime
from fastapi import APIRouter, Request, Response
from fastapi.responses import JSONResponse
from helicase.tabmodules.users import UserDB
from helicase.database.models import BruteForceLog


BF_MAX_RETRIES: int = 3
BF_INTERVAL: int = 15*60


def check_bruteforce(req: Request) -> bool:
	now = int(datetime.datetime.now().timestamp())
	client = req.client.host
	time_limit = now - BF_INTERVAL
	attempts = BruteForceLog.select().where((BruteForceLog.addr == client) & (BruteForceLog.stamp > time_limit))
	if len(attempts) >= BF_MAX_RETRIES:
		return False
	return True


def save_bruteforce(req: Request):
	now = int(datetime.datetime.now().timestamp())
	client = req.client.host
	row = BruteForceLog(addr=client, stamp=now)
	row.save()


def clear_bruteforce(req: Request):
	now = int(datetime.datetime.now().timestamp())
	client = req.client.host
	time_limit = now - BF_INTERVAL
	to_clear = BruteForceLog.select().where((BruteForceLog.addr == client) | (BruteForceLog.stamp < time_limit))
	for i in to_clear:
		i.delete_instance()


def session_router() -> APIRouter:

	router = APIRouter()

	@router.post("/login", response_class=JSONResponse)
	async def login_handler(request: Request):
		fdata = dict(await request.form())
		token, expires = UserDB.check_credentials(fdata['username'], fdata['passwd'])
		if (token is not None) and check_bruteforce(request):
			clear_bruteforce(request)
			resp = Response(
				content=json.dumps({"status": "ok"}),
				status_code=200,
				media_type="application/json"
			)
			resp.set_cookie(key="helicase", value=token, expires=expires, max_age=expires)
			return resp
		else:
			save_bruteforce(request)
			return Response(
				content=json.dumps({"error": "access denied"}),
				status_code=403,
				media_type="application/json"
			)

	@router.get("/logout", response_class=JSONResponse)
	@router.post("/logout", response_class=JSONResponse)
	async def logout_handler():
		resp = Response(
			content=json.dumps({"status": "ok"}),
			status_code=200,
			media_type="application/json"
		)
		resp.delete_cookie(key="helicase")
		return resp

	return router
