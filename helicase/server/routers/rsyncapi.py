import json
from typing import Optional
from fastapi import APIRouter, Request, Response
from fastapi.responses import JSONResponse
from helicase.tabmodules.rsync import RSyncRunner
from helicase.server.decorators import login_required, check_job_running


def rsync_router(guibypass: bool) -> APIRouter:

	router = APIRouter()

	@router.get("/api/rsync/{rsyncid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	async def get_rsync_json(request: Request, rsyncid: int):
		return Response(
			content=RSyncRunner.get_rsync_json(rsyncid),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/rsync", response_class=JSONResponse)
	@router.post("/api/rsync/{rsyncid}", response_class=JSONResponse)
	@check_job_running
	@login_required(bypass=guibypass)
	async def edit_rsync(request: Request, rsyncid: Optional[int] = None):
		fdata = dict(await request.form())
		RSyncRunner.edit_rsync(formdata=fdata, rsyncid=rsyncid)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.delete("/api/rsync/{rsyncid}", response_class=JSONResponse)
	@login_required(bypass=guibypass)
	@check_job_running
	async def rm_rsync(request: Request, rsyncid: int):
		in_use = RSyncRunner.rm_rsync(rsyncid=rsyncid)
		if in_use is not None:
			in_use = ", ".join(in_use)
			return Response(
				content=json.dumps({"error": "Still in use by jobs: {}".format(in_use)}),
				status_code=513,
				media_type="application/json"
			)
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	return router