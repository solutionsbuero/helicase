import json
from fastapi import APIRouter, Request, Response
from helicase.server.decorators import login_required
from helicase.tabmodules.ssh import KeyManager


def ssh_router(guibypass: bool) -> APIRouter:

	router = APIRouter()

	@router.get("/api/keys/create_private")
	@login_required(bypass=guibypass)
	async def create_public(request: Request):
		KeyManager.create_private_key()
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.get("/api/keys/create_public")
	@login_required(bypass=guibypass)
	async def create_public(request: Request):
		KeyManager.create_public_key()
		return Response(
			content=json.dumps({"state": "ok"}),
			status_code=200,
			media_type="application/json"
		)

	@router.post("/api/keys/ssh_copy_id")
	@login_required(bypass=guibypass)
	async def copy_id(request: Request):
		fdata = dict(await request.form())
		rc, stdout, stderr = KeyManager.copy_id(
			fdata["username"],
			fdata["hostname"],
			int(fdata["port"]),
			fdata["passwd"]
		)
		if rc == 0:
			return Response(
				content=json.dumps({"stdout": stdout}),
				status_code=200,
				media_type="application/json"
			)
		else:
			return Response(
				content=json.dumps({"rc": rc, "stdout": stdout, "stderr": stderr}),
				status_code=512,
				media_type="application/json"
			)

	return router
