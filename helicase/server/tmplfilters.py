import datetime
from dateutil import tz


def stringtime(stamp: int) -> str:
	local = tz.tzlocal()
	dt = datetime.datetime.fromtimestamp(stamp, tz=local)
	return dt.strftime('%d.%m.%Y %H:%M %Z')
