import os
import pwd
from dataclasses import dataclass
from typing import Optional
from fastapi import APIRouter, Request, Response
from helicase.tabmodules.users import UserDB
from helicase.tabmodules.smtp import EMailDB
from helicase.tabmodules.mysql import DumpDB
from helicase.tabmodules.rsync import RSyncRunner
from helicase.tabmodules.ssh import KeyManager
from helicase.tabmodules.command import CmdDB
from helicase.tabmodules.jobs import JobManager
from helicase.sharedobjects import SharedObjects
from helicase.database.connect import DBPath


@dataclass
class Tab:
	id: str
	link: str
	template_file: str
	request: Request
	data: Optional[dict] = None
	init: bool = False


class TabFactory:

	def __init__(self, shared_obj: SharedObjects, gui: bool = False):
		self.templates = shared_obj.jinja_templates
		self.gui = gui

	def render_html(self, tab: Tab) -> dict:
		link = self.templates.TemplateResponse("tablink.html", {"request": tab.request, "tab": tab})
		content = self.templates.TemplateResponse(tab.template_file, {"request": tab.request, "tab": tab, "data": tab.data})
		return {"link": link.body.decode(), "content": content.body.decode()}

	def render_login(self, req: Request, init: bool = True) -> dict:
		ltab = Tab(
			id="logintab",
			link="Login",
			template_file="login.html",
			request=req,
			init=init
		)
		return self.render_html(ltab)

	def render_joblist(self, req: Request) -> dict:
		jtab = Tab(
			id="jobtab",
			link="Jobs",
			template_file="jobs.html",
			request=req,
			data={"jobs": JobManager.list_jobs(), "mail": EMailDB.list_mail()}
		)
		return self.render_html(jtab)

	def render_rsynclist(self, req: Request) -> dict:
		rtab = Tab(
			id="rsynctab",
			link="rsync",
			template_file="rsync.html",
			request=req,
			data={'rsync': RSyncRunner.list_rsync(), 'patterns': RSyncRunner.USUAL_PATTERNS}
		)
		return self.render_html(rtab)

	def render_userlist(self, req: Request) -> dict:
		if self.gui:
			return {"link": "", "content": ""}
		utab = Tab(
			id="usertab",
			link="Users",
			template_file="users.html",
			request=req,
			data={'users': UserDB.list_users()}
		)
		return self.render_html(utab)

	def render_about(self, req: Request) -> dict:
		atab = Tab(
			id="abouttab",
			link="About",
			template_file="about.html",
			request=req
		)
		return self.render_html(atab)

	def render_mail(self, req: Request) -> dict:
		mtab = Tab(
			id="mailtab",
			link="SMTP",
			template_file="mail.html",
			request=req,
			data={'mail': EMailDB.list_mail()}
		)
		return self.render_html(mtab)

	def render_dumps(self, req: Request) -> dict:
		dtab = Tab(
			id="dumptab",
			link="MySQL",
			template_file="dump.html",
			request=req,
			data={'dumps': DumpDB.list_dumps()}
		)
		return self.render_html(dtab)

	def render_ssh(self, req: Request) -> dict:
		stab = Tab(
			id="sshtab",
			link="SSH",
			template_file="ssh.html",
			request=req,
			data={'keys': KeyManager.check_keys_dict(), 'user': pwd.getpwuid(os.getuid()).pw_name}
		)
		return self.render_html(stab)

	def render_shutdown(self, req: Request) -> dict:
		sdtab = Tab(
			id="shutdowntab",
			link="Shutdown",
			template_file="shutdown.html",
			request=req,
			data={'user': os.getlogin()}
		)
		return self.render_html(sdtab)

	def render_command(self, req: Request):
		ctab = Tab(
			id="cmdtab",
			link="CMD",
			template_file="command.html",
			request=req,
			data={'commands': CmdDB.list_cmd(), 'filename': str(DBPath.get_cmd_file())}
		)
		print(ctab.data["filename"])
		return self.render_html(ctab)

	def render_statelist(self, req: Request, init: bool = True) -> dict:
		stab = Tab(
			id="statetab",
			link="State",
			template_file="state.html",
			request=req,
			init=init,
			data={"jobs": JobManager.list_jobs()}
		)
		return self.render_html(stab)

	def render_logout(self) -> dict:
		link = "" if self.gui else '<a class="tablinks" href="javascript: logout();">Logout</a>'
		return {"link": link, "content": ""}
