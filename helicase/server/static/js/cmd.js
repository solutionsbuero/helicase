////////////////////////////
// cmd callbacks & functions
////////////////////////////

function cmdedit_success(data) {
    partial_reload("#cmdlist_container", "/part/cmdlist");
}

function cmdedit_error(xhr) {
    errormsg("Unhandled error " + xhr.responseText);
}

function rm_cmd(cmdid) {
    conf = confirm("Remove command?");
    if(conf) rm_cmd_confirmed(cmdid);
}

function rm_cmd_confirmed(cmdid) {
    $.ajax({
        type: "DELETE",
        url: "/api/cmd/" + cmdid,
        dataType: 'json',
        success: function(data) {
            partial_reload("#cmdlist_container", "/part/cmdlist");
            successmsg("Command removed");
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function edit_cmd(cmdid=null) {
    if (cmdid == null) {
        $("#edit_cmd_form").attr("action", "/api/cmd");
        $('#edit_cmd_form input[name ="human"]').val("");
        $('#edit_cmd_form input[name ="cmd"]').val("");
        $('#edit_cmd_form input[type ="submit"]').val("Add command");
    }
    else {
        $.ajax({
            type: "GET",
            url: "/api/cmd/" + cmdid,
            dataType: 'json',
            success: function(data) {
                $("#edit_cmd_form").attr("action", "/api/cmd/" + cmdid);
                $('#edit_cmd_form input[name ="human"]').val(data.human);
                $('#edit_cmd_form input[name ="cmd"]').val(data.cmd);
                $('#edit_cmd_form input[type ="submit"]').val("Edit command");
            },
            error: function(xhr) {
                errormsg("Unhandled error " + xhr.responseText);
            }
        });
    }
}
