
/////////////////////////////////
// Submit various forms over ajax
/////////////////////////////////

$(document).on('submit', '.dataentry', function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var url = form.attr('action');
    var formid = form.attr('id');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), // serializes the form's elements.
        dataType: 'json',
        success: function(data) {
            if(formid == "loginform") login_success(data);
            if(formid == "edit_user_form") useredit_success(data);
            if(formid == "edit_mail_form") mailedit_success(data);
            if(formid == "test_mail_form") mailtest_success(data);
            if(formid == "edit_dump_form") dumpedit_success(data);
            if(formid == "test_dump_form") dumptest_success(data);
            if(formid == "ssh_copy_id_form") sshcopy_success(data);
            if(formid == "edit_rsync_form") rsyncedit_success(data);
            if(formid == "edit_cmd_form") cmdedit_success(data);
            if(formid == "edit_job_form") jobedit_success(data);
            if(formid == "edit_cron_form") cronedit_success(data);
            //if(formid == "unlock_shutdown_form") unlock_shutdown_success(data);
        },
        error: function(xhr) {
            if(formid == "loginform") login_error(xhr);
            if(formid == "edit_user_form") useredit_error(xhr);
            if(formid == "edit_mail_form") mailedit_error(xhr);
            if(formid == "test_mail_form") mailtest_error(xhr);
            if(formid == "edit_dump_form") dumpedit_error(xhr);
            if(formid == "test_dump_form") dumptest_error(xhr);
            if(formid == "ssh_copy_id_form") sshcopy_error(xhr);
            if(formid == "edit_rsync_form") rsyncedit_error(xhr);
            if(formid == "edit_cmd_form") cmdedit_error(xhr);
            if(formid == "edit_job_form") jobedit_error(xhr);
            if(formid == "edit_cron_form") cronedit_error(xhr);
            //if(formid == "unlock_shutdown_form") unlock_shutdown_error(xhr);
        }
    });
});
