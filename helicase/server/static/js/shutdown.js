////////////////////////////
// Shutdown callbacks & functions
////////////////////////////

function unlock_shutdown_success(data) {
    successmsg("Unlock successful");
}

function unlock_shutdown_error(xhr) {
    if(xhr.status == 512) {
        return_data = JSON.parse(xhr.responseText);
        errormsg("Unlock failed with return code " + return_data.rc + ": " + return_data.stderr);
    }
    else {
        errormsg("Unhandled error " + xhr.responseText);
    }
}

function test_shutdown() {
    conf = confirm("If this actually works, it will shutdown your computer. Proceed?");
    if (conf) {
        test_shutdown_conf1();
    }
}

function test_shutdown_conf1() {
    conf = confirm("I mean..., really?");
    if (conf) {
        test_shutdown_conf2();
    }
}

function test_shutdown_conf2() {
    $.ajax({
        type: "GET",
        url: "/api/shutdown/test",
        dataType: 'json',
        success: function(data) {
            successmsg("Shutting down now")
        },
        error: function(xhr) {
            if(xhr.status == 512) {
                return_data = JSON.parse(xhr.responseText);
                errormsg("Unlock failed with return code " + return_data.rc + ": " + return_data.stderr);
            }
            else {
                errormsg("Unhandled error " + xhr.responseText);
            }
        }
    });
}
