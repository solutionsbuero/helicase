function partial_reload(element_id, endpoint, additional_data = null) {

    if(additional_data == null) serialdata = {data: null};
    else serialdata = {data: JSON.stringify(additional_data)};
    $.ajax({
        type: "POST",
        url: endpoint,
        data: serialdata, // serializes the form's elements.
        dataType: 'json',
        success: function(data) {
            $(element_id).html(data);
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}
