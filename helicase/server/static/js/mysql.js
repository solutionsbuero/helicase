////////////////////////////
// MySQL Dump edit Callbacks
////////////////////////////

function dumpedit_success(data) {
    partial_reload("#dumplist_container", "/part/dumptable");
    partial_reload(".dump_drop_container", "/part/dumpdrop");
    successmsg("DB details saved");
}

function dumpedit_error(xhr) {
    errormsg("Unhandled error " + xhr.responseText);
}

function rm_dump(dumpid) {
    conf = confirm("Remove dump?");
    if(conf) rm_dump_confirmed(dumpid);
}

function rm_dump_confirmed(dumpid) {
    $.ajax({
        type: "DELETE",
        url: "/api/dump/" + dumpid,
        dataType: 'json',
        success: function(data) {
            partial_reload("#dumplist_container", "/part/dumptable");
            partial_reload(".dump_drop_container", "/part/dumpdrop");
            successmsg("DB dump removed");
        },
        error: function(xhr) {
            if(xhr.status == 513) {
                return_data = JSON.parse(xhr.responseText);
                errormsg(return_data.error);
            }
            else {
                errormsg("Unhandled error " + xhr.responseText);
            }
        }
    });
}

function edit_dump(dumpid=null) {
    if (dumpid == null) {
        $("#edit_dump_form").attr("action", "/api/dump");
        $('#edit_dump_form input[name ="host"]').val("");
        $('#edit_dump_form input[name ="port"]').val("");
        $('#edit_dump_form input[name ="username"]').val("");
        $('#edit_dump_form input[name ="passwd"]').val("");
        $('#edit_dump_form input[name ="dbname"]').val("");
        $('#edit_dump_form input[name ="dumppath"]').val("");
        $('#edit_dump_form input[type ="submit"]').val("Add DB dump");
    }
    else {
        $.ajax({
            type: "GET",
            url: "/api/dump/" + dumpid,
            dataType: 'json',
            success: function(data) {
                $("#edit_dump_form").attr("action", "/api/dump/" + dumpid);
                $('#edit_dump_form input[name ="host"]').val(data.host);
                $('#edit_dump_form input[name ="port"]').val(data.port);
                $('#edit_dump_form input[name ="username"]').val(data.username);
                $('#edit_dump_form input[name ="passwd"]').val(data.password);
                $('#edit_dump_form input[name ="dbname"]').val(data.dbname);
                $('#edit_dump_form input[name ="dumppath"]').val(data.dumppath);
                $('#edit_dump_form input[type ="submit"]').val("Modify DB dump");
            },
            error: function(xhr) {
                errormsg("Unhandled error " + xhr.responseText);
            }
        });
    }
}

function dumptest_success(data) {
    successmsg("Dump successful");
}

function dumptest_error(xhr) {
    if(xhr.status == 512) {
        return_data = JSON.parse(xhr.responseText);
        errormsg("Dump failed with return code " + return_data.rc + ": " + return_data.stderr);
    }
    else {
        errormsg("Unhandled error " + xhr.responseText);
    }
}
