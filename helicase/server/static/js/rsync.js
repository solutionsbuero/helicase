////////////////////////////
// rsync Submits & Callbacks
////////////////////////////

function rsyncedit_success(data) {
    partial_reload("#rsynclist_container", "/part/rsynclist");
    successmsg("Command updated");
}

function rsyncedit_error(xhr) {
    errormsg("Unhandled error " + xhr.responseText);
}

function rm_rsync(rsyncid) {
    conf = confirm("Remove command?");
    if(conf) rm_rsync_confirmed(rsyncid);
}

function rm_rsync_confirmed(rsyncid) {
    $.ajax({
        type: "DELETE",
        url: "/api/rsync/" + rsyncid,
        dataType: 'json',
        success: function(data) {
            partial_reload("#rsynclist_container", "/part/rsynclist");
            successmsg("Command removed");
        },
        error: function(xhr) {
            if(xhr.status == 513) {
                return_data = JSON.parse(xhr.responseText);
                errormsg(return_data.error);
            }
            else {
                errormsg("Unhandled error " + xhr.responseText);
            }
        }
    });
}

function edit_rsync(rsyncid=null) {
    if (rsyncid == null) {
        $("#edit_rsync_form").attr("action", "/api/rsync");
        $('#edit_rsync_form input[name ="human"]').val("");
        $('#edit_rsync_form input[name ="flags"]').val("");
        $('#edit_rsync_form input[name ="source"]').val("");
        $('#edit_rsync_form input[name ="destination"]').val("");
        $('#edit_rsync_form input[type ="submit"]').val("Add rsync cmd");
    }
    else {
        $.ajax({
            type: "GET",
            url: "/api/rsync/" + rsyncid,
            dataType: 'json',
            success: function(data) {
                $("#edit_rsync_form").attr("action", "/api/rsync/" + rsyncid);
                $('#edit_rsync_form input[name ="human"]').val(data.human);
                $('#edit_rsync_form input[name ="flags"]').val(data.flags);
                $('#edit_rsync_form input[name ="source"]').val(data.source);
                $('#edit_rsync_form input[name ="destination"]').val(data.destination);
                $('#edit_rsync_form input[type ="submit"]').val("Edit rsync cmd");
            },
            error: function(xhr) {
                errormsg("Unhandled error " + xhr.responseText);
            }
        });
    }
}

function rsync_apply_pattern() {
    $('#edit_rsync_form input[name ="flags"]').val(
        $('#edit_rsync_form select[name ="usual_patterns"]').val()
    );
}
