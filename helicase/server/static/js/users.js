////////////////////////////////////
// User editor Callbacks and submits
////////////////////////////////////

function useredit_success(data) {
    partial_reload("#userlist_container", "/part/usertable");
    successmsg("User data saved");
}

function useredit_error(xhr) {
    if(xhr.status == 400) {
        feedback = JSON.parse(xhr.responseText);
        errormsg(feedback.error);
    }
    else {
        errormsg("Unhandled error " + xhr.responseText);
    }
}

function rm_user(userid) {
    conf = confirm("Remove user?");
    if(conf) rm_user_confirmed(userid);
}

function rm_user_confirmed(userid) {
    $.ajax({
        type: "DELETE",
        url: "/api/user/" + userid,
        dataType: 'json',
        success: function(data) {
            partial_reload("#userlist_container", "/part/usertable");
            successmsg("User removed");
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function edit_user(userid=null) {
    if (userid == null) {
        $("#edit_user_form").attr("action", "/api/user");
        $('#edit_user_form input[name ="username"]').val("").attr("readonly", false);
        $('#edit_user_form input[type ="submit"]').val("Create user");
    }
    else {
        $.ajax({
            type: "GET",
            url: "/api/user/" + userid,
            dataType: 'json',
            success: function(data) {
                $("#edit_user_form").attr("action", "/api/user/" + userid);
                $('#edit_user_form input[name ="username"]').val(data.username).attr("readonly", true);
                $('#edit_user_form input[type ="submit"]').val("Modify user");
            },
            error: function(xhr) {
                errormsg("Unhandled error " + xhr.responseText);
            }
        });
    }
}
