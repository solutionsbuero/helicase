/////////////////////////
// Login/Logout Callbacks
/////////////////////////

function login_success(data) {
    load_content();
}

function login_error(xhr) {
    if(xhr.status == 403) {
        errormsg("Access denied");
    }
    else {
        errormsg("Unhandled error " + xhr.responseText);
    }
}

function logout() {
    $.ajax({
        type: "GET",
        url: "/logout",
        dataType: 'json',
        success: function(data) {
            stop_short_poll();
            load_content();
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}
