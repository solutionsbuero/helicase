function jobedit_success(data) {
    partial_reload("#joblist_container", "/part/joblist");
    successmsg("Job updated");
}

function jobedit_error(xhr) {
    errormsg("Unhandled error " + xhr.responseText);
}

function rm_job(jobid) {
    conf = confirm("Remove job?");
    if(conf) rm_job_confirmed(jobid);
}

function rm_job_confirmed(jobid) {
    $.ajax({
        type: "DELETE",
        url: "/api/job/" + jobid,
        dataType: 'json',
        success: function(data) {
            partial_reload("#joblist_container", "/part/joblist");
            successmsg("Job removed");
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function edit_job(jobid=null) {
    if (jobid == null) {
        $("#edit_job_form").attr("action", "/api/job");
        $('#edit_job_form input[name ="human"]').val("");
        $('#edit_job_form select[name ="smtp"]').val("null");
        $('#edit_job_form select[name ="smtp_send_success"]').val("");
        $('#edit_job_form input[name ="smtp_to"]').val("");
        $('#edit_job_form select[name ="shutdown"]').val("");
        $('#edit_job_form input[type ="submit"]').val("Add job");
        // Update matchlist:
        $('#matchlist_container').html("");
        $('#cronjob_container').html("");
    }
    else {
        $.ajax({
            type: "GET",
            url: "/api/job/" + jobid,
            dataType: 'json',
            success: function(data) {
                partial_reload("#matchlist_container", "/part/matchlist/" + jobid);
                partial_reload("#cronjob_container", "/part/cron/" + jobid);
                $("#edit_job_form").attr("action", "/api/job/" + jobid);
                $('#edit_job_form input[name ="human"]').val(data.human);
                if(data.smtp == null) $('#edit_job_form select[name ="smtp"]').val("null");
                else $('#edit_job_form select[name ="smtp"]').val(data.smtp.id);
                if(data.smtp_send_success) $('#edit_job_form select[name ="smtp_send_success"]').val("True");
                else $('#edit_job_form select[name ="smtp_send_success"]').val("");
                $('#edit_job_form input[name ="smtp_to"]').val(data.smtp_to);
                if(data.shutdown) $('#edit_job_form select[name ="shutdown"]').val("True");
                else $('#edit_job_form select[name ="shutdown"]').val("");
                $('#edit_job_form input[type ="submit"]').val("Edit job");
            },
            error: function(xhr) {
                errormsg("Unhandled error " + xhr.responseText);
            }
        });
    }
}

function add_mysql_assoc(jobid, dumpid) {
    $.ajax({
        type: "POST",
        url: "/api/job/" + jobid + "/mysqlassoc/" + dumpid,
        dataType: 'json',
        success: function(data) {
            partial_reload("#matchlist_container", "/part/matchlist/" + jobid);
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function rm_mysql_assoc(jobid, associd) {
    $.ajax({
        type: "DELETE",
        url: "/api/job/" + jobid + "/mysqlassoc/" + associd,
        dataType: 'json',
        success: function(data) {
            partial_reload("#matchlist_container", "/part/matchlist/" + jobid);
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function mv_mysql_assoc(jobid, associd, direction) {
    $.ajax({
        type: "POST",
        url: "/api/job/" + jobid + "/mysqlassoc/" + associd + "/mv/" + direction,
        dataType: 'json',
        success: function(data) {
            partial_reload("#matchlist_container", "/part/matchlist/" + jobid);
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function add_rsync_assoc(jobid, cmdid) {
    $.ajax({
        type: "POST",
        url: "/api/job/" + jobid + "/rsyncassoc/" + cmdid,
        dataType: 'json',
        success: function(data) {
            partial_reload("#matchlist_container", "/part/matchlist/" + jobid);
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function rm_rsync_assoc(jobid, associd) {
    $.ajax({
        type: "DELETE",
        url: "/api/job/" + jobid + "/rsyncassoc/" + associd,
        dataType: 'json',
        success: function(data) {
            partial_reload("#matchlist_container", "/part/matchlist/" + jobid);
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function mv_rsync_assoc(jobid, associd, direction) {
    $.ajax({
        type: "POST",
        url: "/api/job/" + jobid + "/rsyncassoc/" + associd + "/mv/" + direction,
        dataType: 'json',
        success: function(data) {
            partial_reload("#matchlist_container", "/part/matchlist/" + jobid);
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function add_cmd_assoc(jobid, cmdid, phase) {
    $.ajax({
        type: "POST",
        url: "/api/job/" + jobid + "/cmdassoc/" + cmdid + "/phase/" + phase,
        dataType: 'json',
        success: function(data) {
            partial_reload("#matchlist_container", "/part/matchlist/" + jobid);
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function rm_cmd_assoc(jobid, associd, phase) {
    $.ajax({
        type: "DELETE",
        url: "/api/job/" + jobid + "/cmdassoc/" + associd,
        dataType: 'json',
        success: function(data) {
            partial_reload("#matchlist_container", "/part/matchlist/" + jobid);
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function mv_cmd_assoc(jobid, associd, direction, phase) {
    $.ajax({
        type: "POST",
        url: "/api/job/" + jobid + "/cmdassoc/" + associd + "/mv/" + direction + "/phase/" + phase,
        dataType: 'json',
        success: function(data) {
            partial_reload("#matchlist_container", "/part/matchlist/" + jobid);
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}
