////////////////////////////////////
// SMTP editor Callbacks and submits
////////////////////////////////////

function mailedit_success(data) {
    partial_reload("#maillist_container", "/part/mailtable");
    partial_reload(".mail_drop_container", "/part/maildrop");
    successmsg("Account updated");
}

function mailedit_error(xhr) {
    if(xhr.status == 400) {
        feedback = JSON.parse(xhr.responseText);
        errormsg(feedback.error);
    }
    else {
        errormsg("Unhandled error " + xhr.responseText);
    }
}

function rm_mail(mailid) {
    conf = confirm("Remove account?");
    if(conf) rm_mail_confirmed(mailid);
}

function rm_mail_confirmed(mailid) {
    $.ajax({
        type: "DELETE",
        url: "/api/mail/" + mailid,
        dataType: 'json',
        success: function(data) {
            partial_reload("#maillist_container", "/part/mailtable");
            partial_reload(".mail_drop_container", "/part/maildrop");
            successmsg("Account removed");
        },
        error: function(xhr) {
            if(xhr.status == 513) {
                return_data = JSON.parse(xhr.responseText);
                errormsg(return_data.error);
            }
            else {
                errormsg("Unhandled error " + xhr.responseText);
            }
        }
    });
}

function edit_mail(mailid=null) {
    if (mailid == null) {
        $("#edit_mail_form").attr("action", "/api/mail");
        $('#edit_mail_form input[name ="host"]').val("");
        $('#edit_mail_form input[name ="port"]').val("");
        $('#edit_mail_form select[name ="ssl"]').val("True");
        $('#edit_mail_form input[name ="username"]').val("");
        $('#edit_mail_form input[name ="passwd"]').val("");
        $('#edit_mail_form input[name ="from_addr"]').val("");
        $('#edit_mail_form input[type ="submit"]').val("Add account");
        console.log($('#edit_mail_form select[name ="ssl"]').val());
    }
    else {
        $.ajax({
            type: "GET",
            url: "/api/mail/" + mailid,
            dataType: 'json',
            success: function(data) {
                $("#edit_mail_form").attr("action", "/api/mail/" + mailid);
                $('#edit_mail_form input[name ="host"]').val(data.host);
                $('#edit_mail_form input[name ="port"]').val(data.port);
                if(data.ssl) $('#edit_mail_form select[name ="ssl"]').val("True");
                else $('#edit_mail_form select[name ="ssl"]').val("");
                $('#edit_mail_form input[name ="username"]').val(data.username);
                $('#edit_mail_form input[name ="passwd"]').val(data.password);
                $('#edit_mail_form input[name ="from_addr"]').val(data.from_addr);
                $('#edit_mail_form input[type ="submit"]').val("Modify account");
            },
            error: function(xhr) {
                errormsg("Unhandled error " + xhr.responseText);
            }
        });
    }
}

function mailtest_success(data) {
    successmsg("Test sent");
}

function mailtest_error(xhr) {
    errormsg("Unhandled error " + xhr.responseText);
}
