var short_poll = false;
var state_data = false;

function start_short_poll() {
    short_poll = setInterval(short_poll_data, 1000);
}

function stop_short_poll() {
    if(short_poll) {
        clearInterval(short_poll);
        short_poll = false;
    }
}

function short_poll_data() {
    $.ajax({
        type: "POST",
        url: "/part/statelist",
        dataType: 'json',
        success: function(data) {
            if(data != state_data) {
                $("#statelist_container").html(data);
                state_data = data;
            }
        },
        error: function(xhr) {
            if(xhr.status == 403) logout();
            else errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function toggle_stdout(jobid) {
    sel = $("#stdout_" + jobid);
    toggle_output(sel);

}

function toggle_stderr(jobid) {
    sel = $("#stderr_" + jobid);
    toggle_output(sel);
}

function toggle_output(sel) {
    if(sel.is(':hidden')) {
        sel.slideDown();
    }
    else {
        sel.slideUp();
    }
}

function start_job(jobid) {
    $.ajax({
        type: "POST",
        url: "/api/evoke/" + jobid,
        dataType: 'json',
        success: function(data) {
            successmsg(data.state);
            partial_reload("#statelist_container", "/part/statelist")
        },
        error: function(xhr) {
            if(xhr.status == 409) {
                return_data = JSON.parse(xhr.responseText);
                errormsg(return_data.state);
            }
            else errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function confirm_run(jobid) {
    check = confirm("This job will shutdown your computer on completion. Proceed?");
    if(check) start_job(jobid);
}
