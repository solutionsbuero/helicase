function load_content() {
    $.ajax({
        type: "GET",
        url: "/tabs/content",
        dataType: "json",
        success: function(result) {
            draw_tabs(result);
        },
        error: function(xhr) {
            if (xhr.status == 403) {
                $.ajax({
                    type: "GET",
                    url: "/tabs/login",
                    dataType: "json",
                    success: function(result) {
                        draw_tabs(result);
                    },
                    error: function(xhr) {
                        errormsg("Error " + xhr.status + ": " + xhr.responseText);
                    }
                });
            }
            else {
                errormsg("Error " + xhr.status + ": " + xhr.responseText);
            }
        }
    });
}

function draw_tabs(tabdata) {
    $(".tabcontainer").html("");
    $("#maincontent").html("");
    for(var i=0; i < tabdata.length; i++) {
        $(".tabcontainer").append(tabdata[i].link);
        $("#maincontent").append(tabdata[i].content);
    }
}

function draw_logout() {
    $(".tabcontainer").append('<a class="tablinks" href="javascript: logout();">Logout</a>');
}

function show_tab(tabid) {
    $('.tabcontent').each(function(i, tabcon) {
        $(tabcon).css('display', 'none');
    });
    $('.tablinks').each(function(i, tablink) {
        $(tablink).removeClass("tabactive");
    });
    $('#' + tabid).css('display', 'block');
    $('#tab_' + tabid).addClass("tabactive");
    //if(tabid == "statetab") start_short_poll();
    //else stop_short_poll();
}
