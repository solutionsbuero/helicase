////////////////////////////
// SSH Keys Submits & Callbacks
////////////////////////////

function ssh_create_public_key() {
    $.ajax({
        type: "GET",
        url: "/api/keys/create_public",
        dataType: 'json',
        success: function(data) {
            partial_reload("#own_key_container", "/part/own_keys");
            successmsg("Key created");
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function ssh_create_private_key() {
    $.ajax({
        type: "GET",
        url: "/api/keys/create_private",
        dataType: 'json',
        success: function(data) {
            partial_reload("#own_key_container", "/part/own_keys");
            successmsg("Key created");
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}

function sshcopy_success(data) {
    successmsg("Copy successful");
}

function sshcopy_error(xhr) {
    if(xhr.status == 512) {
        return_data = JSON.parse(xhr.responseText);
        errormsg("Copy failed with return code " + return_data.rc + ": " + return_data.stderr);
    }
    else {
        errormsg("Unhandled error " + xhr.responseText);
    }
}
