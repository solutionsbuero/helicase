feedback_counter = 0;

// Write successful message
function successmsg(msg) {
    create_feedback_element(msg, false, true);
}

// Write error message
function errormsg(msg) {
    create_feedback_element(msg, true, false);
}

function create_feedback_element(msg, iserror = false, issuccess = false) {
    eid = "feedback_" + feedback_counter.toString()
    feedback_counter++;
    addonclass = "";
    if (issuccess) addonclass = "successmsg";
    if (iserror) addonclass = "errormsg";
    elem_str = `<div id=${eid} class="fbelement ${addonclass}">${msg}</div>`;
    $("#feedback").prepend(elem_str);
    $("#" + eid).hide();
    $("#" + eid).fadeIn(300).delay(1500).fadeOut(300);
}
