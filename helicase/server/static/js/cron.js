function cronedit_success(data) {
    partial_reload("#cronjob_container", "/part/cron/" + data.jobid);
    successmsg("Cron job updated");
    partial_reload("#statelist_container", "/part/statelist");
}

function cronedit_error(xhr) {
    if(xhr.status == 512) {
        return_data = JSON.parse(xhr.responseText);
        errormsg("Cron job invalid");
    }
    else {
        errormsg("Unhandled error " + xhr.responseText);
    }
}

function rm_cron(jobid) {
    $.ajax({
        type: "DELETE",
        url: "/api/cron/" + jobid,
        dataType: 'json',
        success: function(data) {
            successmsg("Cron job removed");
            partial_reload("#cronjob_container", "/part/cron/" + data.jobid);
            partial_reload("#statelist_container", "/part/statelist");
        },
        error: function(xhr) {
            errormsg("Unhandled error " + xhr.responseText);
        }
    });
}
