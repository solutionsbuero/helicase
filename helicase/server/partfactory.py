import os
import json
from dataclasses import dataclass
from typing import Optional
from fastapi.templating import Jinja2Templates
from fastapi import APIRouter, Request, Response
from helicase.tabmodules.users import UserDB
from helicase.tabmodules.smtp import EMailDB
from helicase.tabmodules.mysql import DumpDB
from helicase.tabmodules.rsync import RSyncRunner
from helicase.tabmodules.ssh import KeyManager
from helicase.tabmodules.command import CmdDB
from helicase.tabmodules.jobs import JobManager
from helicase.tabmodules.cron import CronManager
from helicase.sharedobjects import SharedObjects


@dataclass
class UpdatePart:
	template_file: str
	request: Request
	data: Optional[dict] = None


class PartFactory:

	def __init__(self, shared_obj: SharedObjects, gui: bool = False):
		self.templates = shared_obj.jinja_templates
		self.gui = gui

	def render_html(self, upart: UpdatePart) -> str:
		content = self.templates.TemplateResponse(upart.template_file, {"request": upart.request, "data": upart.data})
		return content.body.decode()

	def render_usertable(self, req: Request):
		usertable = UpdatePart(
			template_file="userlist.html",
			request=req,
			data={"users": UserDB.list_users()}
		)
		return self.render_html(usertable)

	def render_mailtable(self, req: Request):
		mailtable = UpdatePart(
			template_file="maillist.html",
			request=req,
			data={'mail': EMailDB.list_mail()}
		)
		return self.render_html(mailtable)

	def render_maildrop(self, req: Request):
		maildrop = UpdatePart(
			template_file="maildrop.html",
			request=req,
			data={'mail': EMailDB.list_mail()}
		)
		return self.render_html(maildrop)

	def render_dumptable(self, req: Request):
		dumptable = UpdatePart(
			template_file="dumplist.html",
			request=req,
			data={'dumps': DumpDB.list_dumps()}
		)
		return self.render_html(dumptable)

	def render_dumpdrop(self, req: Request):
		dumpdrop = UpdatePart(
			template_file="dumpdrop.html",
			request=req,
			data={'dumps': DumpDB.list_dumps()}
		)
		return self.render_html(dumpdrop)

	def render_own_keys(self, req: Request):
		own_keys = UpdatePart(
			template_file="ssh_own_key.html",
			request=req,
			data={'keys': KeyManager.check_keys_dict(), 'user': pwd.getpwuid(os.getuid()).pw_name}
		)
		return self.render_html(own_keys)

	def render_rsyncls(self, req: Request):
		rsyls = UpdatePart(
			template_file="rsynclist.html",
			request=req,
			data={'rsync': RSyncRunner.list_rsync()}
		)
		return self.render_html(rsyls)

	def render_cmdlist(self, req: Request):
		cmdlist = UpdatePart(
			template_file="commandlist.html",
			request=req,
			data={'commands': CmdDB.list_cmd()}
		)
		return self.render_html(cmdlist)

	def render_joblist(self, req: Request):
		joblist = UpdatePart(
			template_file="joblist.html",
			request=req,
			data={"jobs": JobManager.list_jobs()}
		)
		return self.render_html(joblist)

	def render_matchlist(self, req: Request, jobid: int):
		matchlist = UpdatePart(
			template_file="matchlist.html",
			request=req,
			data={
				"job": JobManager.get_job(jobid=jobid),
				"filtered_mysql": JobManager.get_filtered_mysql(jobid=jobid),
				"filtered_rsync": JobManager.get_filtered_rsync(jobid=jobid),
				"filtered_cmd": {
					"before_sql": JobManager.get_filtered_cmd(jobid=jobid, phase="before_sql"),
					"before_rsync": JobManager.get_filtered_cmd(jobid=jobid, phase="before_rsync"),
					"before_shutdown": JobManager.get_filtered_cmd(jobid=jobid, phase="before_shutdown")
				},
				"all_cmd": CmdDB.list_cmd()
			}
		)
		return self.render_html(matchlist)

	def render_cronform(self, req: Request, jobid: int):
		has_cron, cron = CronManager.get_cronjob(jobid)
		cronform = UpdatePart(
			template_file="cron.html",
			request=req,
			data={"jobid": jobid, "has_cron": has_cron, "cron": cron}
		)
		return self.render_html(cronform)

	def render_statelist(self, req: Request):
		statelist = UpdatePart(
			template_file="statelist.html",
			request=req,
			data={"jobs": JobManager.list_jobs()}
		)
		return self.render_html(statelist)
