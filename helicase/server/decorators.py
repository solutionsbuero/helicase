import json
from functools import wraps
from fastapi import Response
from helicase.tabmodules.users import UserDB
from helicase.tabmodules.runjob import JobRunner


def check_job_running(f):
	@wraps(f)
	async def decorated_function(*args, **kwargs):
		running = JobRunner.check_running()
		if running is None:
			return await f(*args, **kwargs)
		else:
			return Response(
				content=json.dumps({"error": "Job in progress"}),
				status_code=514,
				media_type="application/json"
			)
	return decorated_function


def login_required(bypass):
	def login_required_simple(f):
		@wraps(f)
		async def decorated_function(*args, **kwargs):
			try:
				token = kwargs['request'].cookies['helicase']
			except (AttributeError, KeyError):
				token = None
			if bypass or UserDB.check_token(token):
				return await f(*args, **kwargs)
			else:
				return Response(
					content=json.dumps({"error": "access denied"}),
					status_code=403,
					media_type="application/json"
				)
		return decorated_function
	return login_required_simple
