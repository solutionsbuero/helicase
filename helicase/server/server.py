import os
import sys
import threading
import asyncio
import hypercorn.asyncio
import hypercorn.config
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from helicase.server.tmplfilters import stringtime
from helicase.sharedobjects import SharedObjects, InterfaceStyle
from helicase.database.config import ConfigDB

# Import routes
from helicase.server.routers.sites import sites_router
from helicase.server.routers.session import session_router
from helicase.server.routers.userapi import users_router
from helicase.server.routers.partial import partial_router
from helicase.server.routers.mailapi import mail_router
from helicase.server.routers.dumpapi import dump_router
from helicase.server.routers.sshapi import ssh_router
from helicase.server.routers.rsyncapi import rsync_router
from helicase.server.routers.shutdownapi import shutdown_router
from helicase.server.routers.commandapi import command_router
from helicase.server.routers.jobapi import job_router
from helicase.server.routers.cronapi import cron_router



class HelicaseServer:

	# Set directories to serve assets and templates from
	@staticmethod
	def serve_from() -> dict:
		basedir = os.path.dirname(os.path.realpath(__file__))
		templates = os.path.join(basedir, "templates")
		static = os.path.join(basedir, "static")
		return {"static": static, "templates": templates}

	def init_jinja(self) -> Jinja2Templates:
		templates = Jinja2Templates(directory=self.serve_dir['templates'])
		templates.env.filters["stringtime"] = stringtime
		return templates

	def __init__(self, shared_obj: SharedObjects):
		self.serve_dir = self.serve_from()
		self.app = FastAPI()
		self.shared_obj = shared_obj
		self.shared_obj.jinja_templates = self.init_jinja()
		self.bypass = False
		if self.shared_obj.interface_style == InterfaceStyle.WEBVIEW:
			self.bypass = True
		# Database not initialized before changing the thread
		self.fthread = None
		self.config = None
		self.event_loop = None
		self.server_task = None

	def add_routers(self):
		self.app.mount("/static", StaticFiles(directory=self.serve_dir['static']), name="static")
		self.app.include_router(session_router())
		self.app.include_router(sites_router(shared_obj=self.shared_obj, guibypass=self.bypass))
		self.app.include_router(partial_router(shared_obj=self.shared_obj, guibypass=self.bypass))
		self.app.include_router(users_router(guibypass=self.bypass))
		self.app.include_router(mail_router(guibypass=self.bypass))
		self.app.include_router(dump_router(guibypass=self.bypass))
		self.app.include_router(ssh_router(guibypass=self.bypass))
		self.app.include_router(rsync_router(guibypass=self.bypass))
		self.app.include_router(shutdown_router(guibypass=self.bypass))
		self.app.include_router(command_router(guibypass=self.bypass))
		self.app.include_router(job_router(guibypass=self.bypass))
		self.app.include_router(cron_router(guibypass=self.bypass))

	def pre_run(self):
		# On new thread, init database
		self.config = ConfigDB.get_subgroup("server")
		self.add_routers()

	def start(self):
		self.pre_run()
		server_config = hypercorn.config.Config()
		if self.shared_obj.interface_style == InterfaceStyle.DAEMON:
			server_config.bind = ["0.0.0.0:{}".format(self.config["apiport"])]
		elif self.shared_obj.interface_style == InterfaceStyle.WEBVIEW:
			server_config.bind = ["127.0.0.1:{}".format(self.config["apiport"])]
		self.event_loop = asyncio.new_event_loop()
		self.server_task = hypercorn.asyncio.serve(self.app, server_config)
		self.event_loop.run_until_complete(self.server_task)

	def stop(self):
		self.event_loop.stop()
		exit()

	def spawn(self):
		self.fthread = threading.Thread(target=self.start)
		self.fthread.start()
