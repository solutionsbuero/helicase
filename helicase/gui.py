import sys
import webview
import time
from typing import Optional
from http.client import HTTPConnection
from helicase.database.config import ConfigDB
from helicase.sharedobjects import SharedObjects, InterfaceStyle
from helicase.server.server import HelicaseServer


class HelicaseGUI:

	def __init__(self):
		self.shared_obj = SharedObjects(interface_style=InterfaceStyle.WEBVIEW)
		self.server_conf = ConfigDB.get_subgroup("server")
		self.server = HelicaseServer(shared_obj=self.shared_obj)

	@staticmethod
	def url_ok(url, port):
		try:
			conn = HTTPConnection(url, port)
			conn.request('GET', '/')
			r = conn.getresponse()
			return r.status == 200
		except ConnectionError:
			return False

	def frontend_init(self):
		self.shared_obj.main_window = webview.create_window('Helicase', 'http://127.0.0.1:{}'.format(self.server_conf['apiport']))

	def frontend_start(self):
		while not self.url_ok('127.0.0.1', self.server_conf['apiport']):
			time.sleep(0.5)
		webview.start(debug=True)

	def stop_application(self):
		self.server.stop()
		exit()

	# Run application
	def run(self):
		self.frontend_init()
		self.server.spawn()
		self.frontend_start()


if __name__ == "__main__":

	# Run application
	helicase = HelicaseGUI()
	helicase.run()
	helicase.stop_application()
