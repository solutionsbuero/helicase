if __name__ == "__main__":
	print('Package cannot be launched directly. Use "helicase.daemon" to deamonize application for server systems or "helicase.gui" for a GUI on graphical systems.')