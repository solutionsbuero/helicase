import json
# import webview
from typing import Optional, Any
from dataclasses import dataclass, field
from dataclasses_json import dataclass_json
from fastapi.templating import Jinja2Templates


@dataclass(frozen=True)
class InterfaceStyle:
	DAEMON = 0
	WEBVIEW = 1


@dataclass
class SharedObjects:
	main_window: Any = None
	interface_style: int = InterfaceStyle.DAEMON
	jinja_templates: Optional[Jinja2Templates] = None
